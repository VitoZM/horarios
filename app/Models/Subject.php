<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    use HasFactory;

    protected $fillable = [
        'subjectId',
        'name',
        'initials',
        'HT',
        'HP',
        'HL',
        'HI',
        'HEI',
        'TH',
        'CR',
        'description',
        'enabled',
        'semesterId',
    ];

    protected $primaryKey = 'subjectId';
}
