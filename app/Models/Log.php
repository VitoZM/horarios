<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    use HasFactory;

    protected $fillable = [
        'logId',
        'entry',
        'departure',
        'duration',
        'status',
        'userId'
    ];

    public $timestamps = false;

    /*protected $casts = [
        'entry' => 'datetime',
        'departure' => 'datetime'
    ];*/

    protected $primaryKey = 'logId';
}
