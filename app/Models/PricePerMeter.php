<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PricePerMeter extends Model
{
    use HasFactory;

    protected $fillable = [
        'pricePerMeterId',
        'price',
        'initialDate',
        'finalDate'
    ];

    protected $primaryKey = 'pricePerMeterId';
}
