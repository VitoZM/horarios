<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PenaltyHistory extends Model
{
    use HasFactory;

    protected $fillable = [
        'penaltyHistoryId',
        'description',
        'clientId',
        'penaltyTypeId'
    ];

    protected $primaryKey = 'penaltyHistoryId';
}
