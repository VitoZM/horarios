<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $fillable = [
        'clientId',
        'clientNumber',
        'ci',
        'name',
        'lastName',
        'sex',
        'birthDay',
        'address',
        'addressNumber',
        'cellphone',
        'payment',
        'enabled'
    ];

    protected $primaryKey = 'clientId';
}
