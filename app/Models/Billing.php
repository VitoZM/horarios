<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
    use HasFactory;

    protected $fillable = [
        'readingId',
        'pricePerMeter',
        'consumption',
        'billingDate',
        'billingPrice',
        'discountPercentage',
        'totalBillingPrice',
        'status',
        'paymentDate',
        'expirationDate'
    ];

    public $timestamps = false;
    protected $primaryKey = 'readingId';
}
