<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Measurer extends Model
{
    use HasFactory;

    protected $fillable = [
        'measurerId',
        'measurerNumber',
        'description',
        'installedAddress',
        'zone',
        'status',
        'registerDate',
        'clientId'
    ];

    protected $primaryKey = 'measurerId';
}
