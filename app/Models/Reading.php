<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reading extends Model
{
    use HasFactory;

    protected $fillable = [
        'readingId',
        'measurerReading',
        'type',
        'image',
        'description',
        'measurerId',
        'userId'
    ];

    protected $primaryKey = 'readingId';
}
