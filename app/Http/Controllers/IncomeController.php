<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Income;

class IncomeController extends Controller
{
    public static function updateJson($initialDate,$finalDate){
        $incomesJson = Income::select('*')
                    ->where('created_at','>=',$initialDate)
                    ->where('created_at','<=',$finalDate)
                    ->get()
                    ->toJson();
        try{
            $file = fopen("app-assets/data/ingresos-list.json", "w+b");
            fwrite($file, $incomesJson);
            // Fuerza a que se escriban los datos pendientes en el buffer:
            fflush($file);
        }catch (Throwable $t){

        }finally{
            fclose($file);
        }
    }

    public static function create($amount,$modelType,$modelId){
        $income = new Income([
            'amount' => $amount,
            'modelType' => $modelType,
            'modelId' => $modelId,
        ]);
        $income->save();
    }

    public function read(Request $request){
        $initialDate = $request->initialDate;
        $finalDate = $request->finalDate;
        self::updateJson($initialDate,$finalDate);
        $incomes = Income::select('*')
                    ->where('created_at','>=',$initialDate)
                    ->where('created_at','<=',$finalDate)
                    ->get();

        return response()->json(['success' => true,'incomes' => $incomes], 200);
    }
}
