<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Billing;
use App\Models\PricePerMeter;
use App\Models\Reading;
use App\Models\Client;
use App\Models\Discount;
use Illuminate\Support\Facades\DB;

class BillingController extends Controller
{
    public static function updateJson(){
        $billingsJson = Billing::select('*')->get()->toJson();
        try{
            $file = fopen("app-assets/data/deudas-list.json", "w+b");
            fwrite($file, $billingsJson);
            // Fuerza a que se escriban los datos pendientes en el buffer:
            fflush($file);
        }catch (Throwable $t){

        }finally{
            fclose($file);
        }
    }

    public static function create($reading){
        try{
            $date = new \DateTime();
            $now = new \DateTime($date->format("Y-m-d"));
            $pricePerMeter = PricePerMeter::where('initialDate','<=',$now)
                                            ->where('finalDate','>=',$now)
                                            ->get()
                                            ->first();//obtener precio correspondiente de acuerdo a fecha
            $lastReading = Reading::all()
                            ->where('measurerId',$reading->measurerId)
                            ->where('readingId','<>',$reading->readingId)
                            ->last();//obtener penultima lectura del medidor

            if($reading->type == "2"){//caso pronosticado
                $consumption = DB::table('billings')
                                    ->join('readings','readings.readingId','=','billings.readingId')
                                    ->where('readings.measurerId',$reading->measurerId)
                                    ->latest()//cuidado con esta función, ordena según fecha.
                                    ->take(3)
                                    ->get()
                                    ->avg('consumption');//calcula el consumo promedio de 3 meses
            }
            else{//crear consumo restando la ultima con la penultima lectura, si es la primera lectura dicha lectura no se resta
                $consumption = $lastReading != null ? $reading->measurerReading - $lastReading->measurerReading : $reading->measurerReading;
            }
            $lastBilling = Billing::join('readings','readings.readingId','=','billings.readingId')
                                    ->where('measurerId',$reading->measurerId)
                                    ->select('billings.*')
                                    ->get()
                                    ->last();//obtener la ultima boleta del medidor

            $billingDate = new \DateTime();
            $expirationDate = new \DateTime();

            if($lastBilling == null){//si es la primera boleta se obtiene la fecha actual
                $year = $billingDate->format('Y') + 0;
                $month = $billingDate->format('m') - 1;
                $day = $billingDate->format('d') + 0;
            }
            else{//se obtiene el ultimo mes de facturación
                $dateParts = explode("-",$lastBilling->billingDate);
                $year = $dateParts[0] + 0;
                $month = $dateParts[1] + 1;
                $day = $dateParts[2] + 0;
            }

            $billingDate->setDate($year+0,$month,$day+0);
            $expirationDate->setDate($year+0,$month+3,$day+0);

            $billingPrice = $consumption * $pricePerMeter->price;

            $client = Client::join('measurers','measurers.clientId','=','clients.clientId')
                            ->select('clients.*')
                            ->where('measurers.measurerId',$reading->measurerId)
                            ->get()
                            ->first();//obtengo el cliente, para tener la fecha de nacimiento

            $age = self::getAge($client->birthDay);//calcular edad del cliente

            $discount = Discount::all()
                                ->where('minimumAge','<=',$age)
                                ->where('maximumAge','>=',$age)
                                ->first();//obtener descuento si se encuentra en algun rango de edad

            $discountPercentage = $discount != null ? $discount->discountPercentage : 0;//asignar descuento
            $totalBillingPrice = $billingPrice * (1-$discountPercentage/100);//calcular costo final de la boleta

            $expirationDate = $expirationDate->format("Y/m/d");
            $billingDate = $billingDate->format("Y/m/d");
            $billingArray = [
                'readingId' => $reading->readingId,
                'pricePerMeter' => $pricePerMeter->price,
                'consumption' => $consumption,
                'billingDate' => $billingDate,
                'billingPrice' => $billingPrice,
                'discountPercentage' => $discountPercentage,
                'totalBillingPrice' => $totalBillingPrice,
                'status' => '0',
                'expirationDate' => $expirationDate
            ];
            $billing = new Billing($billingArray);//instancio nueva boleta con los datos generados
            $billing->save();//guardar boleta en la base de datos.
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function getAllBillings(Request $request){
        $clientNumber = $request->clientNumber;
        $measurerNumber = $request->measurerNumber;

        $client = Client::join('measurers','measurers.clientId','=','clients.clientId')
                        ->where('clients.clientNumber',$clientNumber)
                        ->where('measurers.measurerNumber',$measurerNumber)
                        ->select('clients.*','measurerNumber')
                        ->get()
                        ->first();

        $billings = Billing::join('readings','readings.readingId','=','billings.readingId')
                            ->join('measurers','measurers.measurerId','=','readings.measurerId')
                            ->join('clients','clients.clientId','measurers.clientId')
                            ->select('billings.*')
                            ->where('measurers.measurerNumber',$measurerNumber)
                            ->where('clients.clientNumber',$clientNumber)
                            ->orderBy('billings.readingId','desc')
                            ->get();

        try{
            $file = fopen("app-assets/data/list-facturasCliente.json", "w+b");
            fwrite($file, $billings->toJson());
            // Fuerza a que se escriban los datos pendientes en el buffer:
            fflush($file);
        }catch (Throwable $t){

        }finally{
            fclose($file);
        }

        return response()->json(['response' => 'success','client' => $client,'billings' => $billings], 200);
    }

    public function getBillings(Request $request){
        $clientId = $request->clientId;
        $measurerId = $request->measurerId;

        $client = Client::join('measurers','measurers.clientId','=','clients.clientId')
                        ->where('clients.clientId',$clientId)
                        ->where('measurers.measurerId',$measurerId)
                        ->select('clients.*','measurerNumber')
                        ->get()
                        ->first();

        $billings = Billing::join('readings','readings.readingId','=','billings.readingId')
                            ->join('measurers','measurers.measurerId','=','readings.measurerId')
                            ->select('billings.*')
                            ->where('measurers.clientId',$clientId)
                            ->where('measurers.measurerId',$measurerId)
                            ->where('billings.status','0')
                            ->get();

        try{
            $file = fopen("app-assets/data/deudas-list.json", "w+b");
            fwrite($file, $billings->toJson());
            // Fuerza a que se escriban los datos pendientes en el buffer:
            fflush($file);
        }catch (Throwable $t){

        }finally{
            fclose($file);
        }

        return response()->json(['response' => 'success','client' => $client,'billings' => $billings], 200);
    }

    public static function getAge($birthDay){
        $now = date("Y-m-d");
        $timeDiff = date_diff(date_create($birthDay),date_create($now));
        return $timeDiff->format('%y');
    }

    public function update(Request $request){
        $checkedBillings = $request->checkedBillings;
        foreach($checkedBillings as $readingId){
            $billing = Billing::all()->where('readingId',$readingId)->first();
            $billing->status = '1';
            $billing->save();
            IncomeController::create($billing->totalBillingPrice,'App\Models\Billing',$billing->readingId);
        }
        return response()->json('success', 200);
    }

    public function read(){
        self::updateJson();
        $billings = Billing::all();
        return response()->json(['response' => 'success','billings' => $billings], 200);
    }
}
