<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RegistrationPrice;

class RegistrationPriceController extends Controller
{

    public function read(){
        $registrationPrice = RegistrationPrice::all()->first();
        return response()->json(['response' => 'success','registrationPrice' => $registrationPrice], 200);
    }

    public function update(Request $request){
        try{
            $clientId = $request->clientId;
            $client = Client::all()->where('clientId',$clientId)->first();
            $client->name = $request->name;
            $client->lastName = $request->lastName;
            $client->address = $request->address;
            $client->addressNumber = $request->addressNumber;
            $client->sex = $request->sex;
            $client->birthDay = $request->birthDay;
            $client->cellphone = $request->cellphone;
            $client->save();
            return response()->json(['response' => 'success'], 200);
        }
        catch (Throwable $t){
            return $t;
        }
    }

    public function delete(Request $request){
        try{
            $clientId = $request->clientId;
            $client = Client::all()->where("clientId",$clientId)->first();
            $client->enabled = "0";
            $client->save();
            return response()->json(['response' => 'success'], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }
}
