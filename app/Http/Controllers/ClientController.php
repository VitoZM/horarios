<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Client;
use App\Models\RegistrationPrice;
use App\Http\Controllers\IncomeController;

class ClientController extends Controller
{
    public static function updateJson(){
        $usersJson = Client::select('*')
                            ->where('enabled','1')
                            ->get()
                            ->toJson();
        try{
            $file = fopen("app-assets/data/clientes-list.json", "w+b");
            fwrite($file, $usersJson);
            // Fuerza a que se escriban los datos pendientes en el buffer:
            fflush($file);
        }catch (Throwable $t){

        }finally{
            fclose($file);
        }
    }

    public static function findByClientNumber(Request $request){
        $clientNumber = $request->clientNumber;
        $client = Client::where('clientNumber',$clientNumber)->first();
        if($client != null)
            return response()->json(['client' => $client], 200);
        else
            return response()->json('Not Found',500);
    }

    public static function findByClientNumberByClientId(Request $request){
        $clientNumber = $request->clientNumber;
        $clientId = $request->clientId;
        $client = Client::where('clientNumber',$clientNumber)
                        ->where('clientId','<>',$clientId)
                        ->first();
        if($client != null)
            return response()->json(['client' => $client], 200);
        else
            return response()->json('Not Found',500);
    }

    public static function findByCi(Request $request){
        $ci = $request->ci;
        $client = Client::where('ci',$ci)->first();
        if($client != null)
            return response()->json(['client' => $client], 200);
        else
            return response()->json('Not Found',500);
    }

    public static function findByCiByClientId(Request $request){
        $ci = $request->ci;
        $clientId = $request->clientId;
        $client = Client::where('ci',$ci)
                        ->where('clientId','<>',$clientId)
                        ->first();
        if($client != null)
            return response()->json(['client' => $client], 200);
        else
            return response()->json('Not Found',500);
    }

    public function create(Request $request){
        try{
            $client = new Client($request->all());
            $registrationPrice = RegistrationPrice::all()->first();
            $client->payment = $registrationPrice->price;
            $client->save();
            $client = Client::all()->last();
            IncomeController::create($client->payment,'App\Models\Client',$client->clientId);
            return response()->json(['response' => 'success','client' => $client], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function read(){
        self::updateJson();
        $clients = Client::select('*')
                            ->where('enabled','1')
                            ->get();
        return response()->json(['response' => 'success','clients' => $clients], 200);
    }

    public function update(Request $request){
        try{
            $clientId = $request->clientId;
            $client = Client::all()->where('clientId',$clientId)->first();
            $client->clientNumber = $request->clientNumber;
            $client->ci = $request->ci;
            $client->name = $request->name;
            $client->lastName = $request->lastName;
            $client->address = $request->address;
            $client->addressNumber = $request->addressNumber;
            $client->sex = $request->sex;
            $client->birthDay = $request->birthDay;
            $client->cellphone = $request->cellphone;
            $client->save();
            return response()->json(['response' => 'success'], 200);
        }
        catch (Throwable $t){
            return $t;
        }
    }

    public function delete(Request $request){
        try{
            $clientId = $request->clientId;
            $client = Client::all()->where("clientId",$clientId)->first();
            $client->enabled = "0";
            $client->save();
            return response()->json(['response' => 'success'], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function quantity(){
        $count = Client::select('*')
                        ->where('enabled','1')
                        ->get()
                        ->count();

        return $count;
    }
}
