<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Semester;

class SemesterController extends Controller
{
    public static function createSemesters($careerId,$n){
        for($i=1; $i<=$n; $i++){
            $semester = new Semester([
                'number' => $i,
                'ordinal' => self::getOrdinal($i),
                'careerId' => $careerId
            ]);
            $semester->save();
        }
    }

    public static function getOrdinal($n){
        switch($n){
            case 1:
                return "PRIMERO";
            case 2:
                return "SEGUNDO";
            case 3:
                return "TERCERO";
            case 4:
                return "CUARTO";
            case 5:
                return "QUINTO";
            case 6:
                return "SEXTO";
            case 7:
                return "SEPTIMO";
            case 8:
                return "OCTAVO";
            case 9:
                return "NOVENO";
            case 10:
                return "DECIMO";
            case 11:
                return "DECIMO PRIMERO";
            case 12:
                return "DECIMO SEGUNDO";
            default:
                return $n;
        }
    }
}
