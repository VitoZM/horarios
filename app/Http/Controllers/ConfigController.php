<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Billing;

class ConfigController extends Controller
{
    public function upload(){
        $file = $_FILES['file'];

        $templocation = $file["tmp_name"];
        $name = $file["name"];
        session_start();
        $_SESSION['file'] = $name;
        /*$nameAsignado = explode(".",$name);
        $extension = $name[count($nameAsignado)-1];
        $hoy = getdate();
        $now = $hoy['year']."-".$hoy['mon']."-".$hoy['mday']." ".$hoy['hours'].".".$hoy['minutes'].".".$hoy['seconds'];*/

        if(!$templocation){
            die("NO SE HA SELECCIONADO NINGUN file");
        }
        if(move_uploaded_file($templocation, "app-assets/images/files/$name")){
            echo "file guardado correctamente";
        }
        else{
            echo "Error al guardar el file";
        }
    }

    public function isUploaded(){
        session_start();
        if(isset($_SESSION['file']))
            return response()->json(['response' => 'success'], 200);
        else
            return response()->json(['response' => 'error'], 201);
    }
}
