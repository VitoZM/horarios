<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reading;
use App\Models\Billing;
use App\Models\Measurer;
use App\Http\Controllers\BillingController;

class ReadingController extends Controller
{
    public static function updateJson(){
        $readingsJson = Reading::join('measurers','measurers.measurerId','=','readings.measurerId')
                                ->join('clients','clients.clientId','=','measurers.clientId')
                                ->join('billings','billings.readingId','=','readings.readingId')
                                ->select('clients.clientNumber','measurers.measurerNumber','readings.*','billings.*')
                                ->get()
                                ->toJson();
        try{
            $file = fopen("app-assets/data/readings-list.json", "w+b");
            fwrite($file, $readingsJson);
            // Fuerza a que se escriban los datos pendientes en el buffer:
            fflush($file);
        }catch (Throwable $t){

        }finally{
            fclose($file);
        }
    }

    public static function updatePendingJson(){
        $date = new \DateTime();
        $now = new \DateTime($date->format("Y-m-01"));
        $now = $now->format("Y-m-d");
        $pendingReadingsJson = Measurer::join('readings','readings.measurerId','measurers.measurerId')
                                        ->join('billings','billings.readingId','readings.readingId')
                                        ->join('clients','clients.clientId','measurers.clientId')
                                        ->select('measurers.measurerId','measurers.measurerNumber','clients.clientNumber','measurers.zone','measurers.installedAddress')
                                        ->where('measurers.status','1')
                                        ->groupBy('measurerId','measurerNumber','clientNumber','zone','installedAddress')
                                        ->havingRaw("MAX(billings.billingDate) < ?",[$now])
                                        ->get();
        try{
            $file = fopen("app-assets/data/lecturasPendientes-list.json", "w+b");
            fwrite($file, $pendingReadingsJson);
            // Fuerza a que se escriban los datos pendientes en el buffer:
            fflush($file);
        }catch (Throwable $t){

        }finally{
            fclose($file);
        }
    }

    public static function readPendingReadings(){
        self::updatePendingJson();
        return response()->json(['success' => true], 200);
    }

    public function create(Request $request){
        try{
            $reading = new Reading($request->all());//Instanciar reading con los parametros enviados
            $lastBilling = Billing::join('readings','readings.readingId','billings.readingId')//obtengo la ultima boleta para validar
                                    ->where('readings.measurerId',$reading->measurerId)
                                    ->select('billings.*')
                                    ->get()
                                    ->last();

            if($lastBilling == null){
                if($reading->type == "2")
                    return response()->json(['success' => false], 502);
                else{
                    $measurer = Measurer::all()->where('measurerId',$reading->measurerId)->first();
                    $lastBillingDate = new \DateTime($measurer->created_at);
                }
            }
            else
                $lastBillingDate = new \DateTime($lastBilling->billingDate);

            $date = new \DateTime();
            $now = new \DateTime($date->format("Y-m-01"));

            if($lastBillingDate >= $now)
                return response()->json(['success' => false], 503);
            $user = auth()->user();//Obtener el usuario autenticado
            $reading->userId = $user->userId;//Asignar usuario a la lectura
            $reading->save();//guardar la lectura
            $reading = Reading::All()
                                ->where('readingId',$reading->readingId)
                                ->first();//Obtener instancia de la lectura recién creada
            BillingController::create($reading);//función que crea la boleta de pago

            if($reading->type == "1"){//Tipo de lectura tomada, entonces guardamos la foto y guardamos el nombre de imagen
                session_start();
                $file = $_SESSION['file'];
                $extension = self::getExtension($file);
                $name = "$reading->readingId.$extension";
                $reading->image = $name;
                rename("app-assets/images/files/$file", "app-assets/images/readings/$name");
                $reading->image = $name;
                session_destroy();
            }
            else{//Tipo pronosticado, entonces se obtiene el consumo pronosticado, luego le sumamos este consumo a la ultima lectura
                $lastBilling = Billing::all()->last();//obtener la boleta recién creada
                $lastReading = Reading::all()
                                ->where('measurerId',$reading->measurerId)
                                ->where('readingId','<>',$reading->readingId)
                                ->last();//obtener la lectura anterior a la actual
                $lastMeasurerReading = $lastReading != null ? $lastReading->measurerReading : 0;//asignar lectura actual del medidor
                $reading->measurerReading = $lastMeasurerReading + $lastBilling->consumption;
            }
            $reading->save();
            return response()->json(['response' => 'success','reading' => $reading], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public static function getExtension($file){
        $fileParts = explode(".", $file);
        return $fileParts[count($fileParts)-1];
    }

    public function read(){
        self::updateJson();
        $readings = Reading::all();
        return response()->json(['response' => 'success','readings' => $readings], 200);
    }

    public function update(Request $request){
        try{
            $clientId = $request->clientId;
            $client = Client::all()->where('clientId',$clientId)->first();
            $client->name = $request->name;
            $client->lastName = $request->lastName;
            $client->address = $request->address;
            $client->addressNumber = $request->addressNumber;
            $client->sex = $request->sex;
            $client->birthDay = $request->birthDay;
            $client->cellphone = $request->cellphone;
            $client->save();
            return response()->json(['response' => 'success'], 200);
        }
        catch (Throwable $t){
            return $t;
        }
    }

    public function delete(Request $request){
        try{
            $clientId = $request->clientId;
            $client = Client::all()->where("clientId",$clientId)->first();
            $client->enabled = "0";
            $client->save();
            return response()->json(['response' => 'success'], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function isValid(Request $request){
        $measurerId = $request->measurerId;
        $measurerReading = $request->measurerReading;
        $lastReading = Reading::all()
                                ->where('measurerId',$measurerId)
                                ->last();

        $lastMeasurerReading = $lastReading->measurerReading;

        if($measurerReading<$lastMeasurerReading)
            return response()->json("error", 201);

        return response()->json("success", 200);
    }
}
