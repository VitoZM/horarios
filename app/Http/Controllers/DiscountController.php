<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Discount;

class DiscountController extends Controller
{
    public static function updateJson(){
        $discountsJson = Discount::all()
                                ->toJson();
        try{
            $file = fopen("app-assets/data/descuento-list.json", "w+b");
            fwrite($file, $discountsJson);
            // Fuerza a que se escriban los datos pendientes en el buffer:
            fflush($file);
        }catch (Throwable $t){

        }finally{
            fclose($file);
        }
    }

    public function read(){
        self::updateJson();
        $discounts = Discount::all();
        return response()->json(['response' => 'success','discounts' => $discounts], 200);
    }

    public function create(Request $request){
        try{
            $discount = new Discount($request->all());
            $lastDiscount = Discount::all()->last();
            $minimumAge = $lastDiscount->maximumAge + 1;
            $discount->minimumAge = $minimumAge;
            $discount->save();
            return response()->json(['response' => 'success','discount' => $discount], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function update(Request $request){
        try{
            $discountId = $request->discountId;
            $discountPercentage = $request->discountPercentage;
            $discount = Discount::all()->where('discountId',$discountId)->first();
            $discount->discountPercentage = $discountPercentage;
            $discount->save();
            return response()->json(['response' => 'success','discount' => $discount], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function validateMaximumAge(Request $request){
        $maximumAge = $request->maximumAge;
        $lastDiscount = Discount::all()->last();
        if($maximumAge <= $lastDiscount->maximumAge)
            return response()->json(['response' => 'error'], 201);
        else
            return response()->json(['response' => 'success'], 200);
    }

    public function getMinimumAge(){
        $lastDiscount = Discount::all()->last();
        $minimumAge = $lastDiscount->maximumAge + 1;
        return response()->json($minimumAge, 200);
    }

    public function findById(Request $request){
        $discountId = $request->discountId;
        $discount = Discount::all()
                                ->where('discountId',$discountId)
                                ->first();

        if($discount != null)
            return response()->json(['response' => 'success', 'discount' => $discount], 200);
        else
            return response()->json(['response' => 'error'], 500);
    }
}
