<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Measurer;
use App\Models\Client;

class MeasurerController extends Controller
{
    public static function updateJson(){
        $measurersJson = Measurer::join('clients','measurers.clientId','=','clients.clientId')
                                ->select('measurers.*','clients.clientNumber')
                                ->get()
                                ->toJson();
        try{
            $file = fopen("app-assets/data/medidor-list.json", "w+b");
            fwrite($file, $measurersJson);
            // Fuerza a que se escriban los datos pendientes en el buffer:
            fflush($file);
        }catch (Throwable $t){

        }finally{
            fclose($file);
        }
    }

    public static function updateOnServiceToOutageJson(){
        $maxBillings = 2;//Aqui podemos cambiar la cantidad de facturas, para que entre en corte
        $measurersJson = Measurer::join('readings','readings.measurerId','measurers.measurerId')
                                    ->join('billings','billings.readingId','readings.readingId')
                                    ->join('clients','clients.clientId','measurers.clientId')
                                    ->where('billings.status','0')
                                    ->where('measurers.status','1')
                                    ->select('measurers.measurerId','measurers.measurerNumber','clients.clientNumber','clients.name','clients.lastName','measurers.zone','measurers.installedAddress','measurers.status','measurers.measurerId')
                                    ->groupBy('measurers.measurerId','measurers.measurerNumber','clients.clientNumber','clients.name','clients.lastName','measurers.zone','measurers.installedAddress','measurers.status','measurers.measurerId')
                                    ->havingRaw("count(measurers.measurerId) > $maxBillings")
                                    ->get()
                                    ->toJson();
        try{
            $file = fopen("app-assets/data/on-service-list.json", "w+b");
            fwrite($file, $measurersJson);
            // Fuerza a que se escriban los datos pendientes en el buffer:
            fflush($file);
        }catch (Throwable $t){

        }finally{
            fclose($file);
        }
    }

    public function readOnServiceToOutage(){
        self::updateOnServiceToOutageJson();
        return response()->json(['success' => true], 200);
    }

    public static function updateOnOutageToServiceJson(){
        $maxBillings = 2;//Aqui podemos cambiar la cantidad de facturas, para que entre en corte
        $measurersJson = Measurer::join('readings','readings.measurerId','measurers.measurerId')
                                    ->join('billings','billings.readingId','readings.readingId')
                                    ->join('clients','clients.clientId','measurers.clientId')
                                    ->where('billings.status','0')
                                    ->where('measurers.status','2')
                                    ->select('measurers.measurerId','measurers.measurerNumber','clients.clientNumber','clients.name','clients.lastName','measurers.zone','measurers.installedAddress','measurers.status','measurers.measurerId')
                                    ->groupBy('measurers.measurerId','measurers.measurerNumber','clients.clientNumber','clients.name','clients.lastName','measurers.zone','measurers.installedAddress','measurers.status','measurers.measurerId')
                                    ->havingRaw("count(measurers.measurerId) <= $maxBillings")
                                    ->get()
                                    ->toJson();

        try{
            $file = fopen("app-assets/data/on-outage-list.json", "w+b");
            fwrite($file, $measurersJson);
            // Fuerza a que se escriban los datos pendientes en el buffer:
            fflush($file);
            return $measurersJson;
        }catch (Throwable $t){

        }finally{
            fclose($file);
        }
    }

    public function readOnOutageToService(){
        $measurersJson = self::updateOnOutageToServiceJson();
        return response()->json(['success' => true, 'measurersJson' => $measurersJson], 200);
        return response()->json(['success' => true], 200);
    }

    public function outage(Request $request){
        $measurerId = $request->measurerId;
        $measurer = Measurer::select('*')
                                ->where('measurerId',$measurerId)
                                ->get()
                                ->first();

        if($measurer->status == '2')
            return response()->json(['success' => false], 200);

        $measurer->status = '2';
        $measurer->save();
        return response()->json(['success' => true], 200);
    }

    public function service(Request $request){
        $measurerId = $request->measurerId;
        $measurer = Measurer::select('*')
                                ->where('measurerId',$measurerId)
                                ->get()
                                ->first();

        if($measurer->status == '1')
            return response()->json(['success' => false], 200);

        $measurer->status = '1';
        $measurer->save();
        return response()->json(['success' => true], 200);
    }

    public static function findByMeasurerNumber(Request $request){
        $measurerNumber = $request->measurerNumber;
        $measurer = Measurer::where('measurerNumber',$measurerNumber)->first();
        if($measurer != null)
            return response()->json(['measurer' => $measurer], 200);
        else
            return response()->json('Not Found',500);
    }

    public static function findByMeasurerNumberByMeasurerId(Request $request){
        $measurerNumber = $request->measurerNumber;
        $measurerId = $request->measurerId;
        $measurer = Measurer::where('measurerNumber',$measurerNumber)
                            ->where('measurerId','<>',$measurerId)
                            ->first();
        if($measurer != null)
            return response()->json(['measurer' => $measurer], 200);
        else
            return response()->json('Not Found',500);
    }

    public static function findById(Request $request){
        $measurerId = $request->measurerId;
        $measurer = Measurer::where('measurerId',$measurerId)->first();
        if($measurer != null)
            return response()->json(['measurer' => $measurer], 200);
        else
            return response()->json('Not Found',500);
    }

    public function create(Request $request){
        try{
            $measurer = new Measurer($request->all());
            $measurer->save();
            return response()->json(['response' => 'success','measurer' => $measurer], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function read(){
        self::updateJson();
        $measurers = Measurer::all();
        return response()->json(['response' => 'success','measurers' => $measurers], 200);
    }

    public function update(Request $request){
        try{
            $measurerId = $request->measurerId;
            $measurer = Measurer::all()->where('measurerId',$measurerId)->first();
            $measurer->clientId = $request->clientId;
            $measurer->measurerNumber = $request->measurerNumber;
            $measurer->zone = $request->zone;
            $measurer->installedAddress = $request->installedAddress;
            //$measurer->status = $request->status;
            $measurer->registerDate = $request->registerDate;
            $measurer->description = $request->description;
            $measurer->save();
            return response()->json(['response' => 'success'], 200);
        }
        catch (Throwable $t){
            return $t;
        }
    }

    public function changeStatus(Request $request){
        try{
            $measurerId = $request->measurerId;
            $status = $request->status;
            $measurer = Measurer::all()->where("measurerId",$measurerId)->first();
            $measurer->status = $status;
            $measurer->save();
            return response()->json(['response' => 'success'], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function quantity(){
        $count = Measurer::select('*')
                        ->get()
                        ->count();

        return $count;
    }
}
