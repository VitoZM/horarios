<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PricePerMeter;

class PricePerMeterController extends Controller
{

    public static function updateJson(){
        $pricePerMetersJson = PricePerMeter::all()
                                    ->toJson();
        try{
            $file = fopen("app-assets/data/prices-list.json", "w+b");
            fwrite($file, $pricePerMetersJson);
            // Fuerza a que se escriban los datos pendientes en el buffer:
            fflush($file);
        }catch (Throwable $t){

        }finally{
            fclose($file);
        }
    }

    public function read(){
        self::updateJson();
        $pricePerMeters = PricePerMeter::all();
        return response()->json(['response' => 'success','pricePerMeters' => $pricePerMeters], 200);
    }

    public function create(Request $request){
        try{
            $pricePerMeter = new PricePerMeter($request->all());
            $lastPricePerMeter = PricePerMeter::all()->last();
            $initialDate = new \DateTime();
            $dateParts = explode("-",$lastPricePerMeter->finalDate);
            $initialDate->setDate($dateParts[0]+0,$dateParts[1]+0,$dateParts[2]+1);
            $initialDate = $initialDate->format('Y-m-d');
            $pricePerMeter->initialDate = $initialDate;
            $pricePerMeter->save();
            return response()->json(['response' => 'success','pricePerMeter' => $pricePerMeter], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function update(Request $request){
        try{
            $pricePerMeterId = $request->pricePerMeterId;
            $price = $request->price;
            $pricePerMeter = PricePerMeter::all()->where('pricePerMeterId',$pricePerMeterId)->first();
            $pricePerMeter->price = $price;
            $pricePerMeter->save();
            return response()->json(['response' => 'success','pricePerMeter' => $pricePerMeter], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function validateFinalDate(Request $request){
        $finalDate = $request->finalDate;
        $finalDate = strtotime($finalDate);
        $lastPricePerMeter = PricePerMeter::all()->last();
        $lastFinalDate = $lastPricePerMeter->finalDate;
        $lastFinalDate = strtotime($lastFinalDate);
        if($finalDate <= $lastFinalDate)
            return response()->json(['response' => 'error'], 201);
        else
            return response()->json(['response' => 'success'], 200);
    }

    public function getInitialDate(){
        $lastPricePerMeter = PricePerMeter::all()->last();
        $initialDate = new \DateTime();
        $dateParts = explode("-",$lastPricePerMeter->finalDate);
        $initialDate->setDate($dateParts[0]+0,$dateParts[1]+0,$dateParts[2]+1);
        $initialDate = $initialDate->format('Y-m-d');
        return response()->json($initialDate, 200);
    }

    public function findById(Request $request){
        $pricePerMeterId = $request->pricePerMeterId;
        $pricePerMeter = PricePerMeter::all()
                                        ->where('pricePerMeterId',$pricePerMeterId)
                                        ->first();

        if($pricePerMeter != null)
            return response()->json(['response' => 'success', 'pricePerMeter' => $pricePerMeter], 200);
        else
            return response()->json(['response' => 'error'], 500);
    }
}
