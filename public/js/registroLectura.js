
isLogged('create-readings');

fillSelectMeasurerId();
fillPendingReadings();

$("#createBtn").on("click", (event) => {
    event.preventDefault();
    if (error()) return;
    create();
});

function readPendingReadings() {
    let url = "api/reading/readPendingReadings";
    $.post({
        url: url,
        async: false,
        success: (response) => {
            console.log(response);
        },
    });
}
function error() {
    let typeError = validateType();
    let imageError = false;
    let measurerReadingError = false;
    let measurerReadingError2 = false;
    if(typeError){
        validateMeasurerId();
        return true;
    }
    else{
        let type = $('#type').val();
        if(type == '1'){
            imageError = validateImage();
            measurerReadingError = validateMeasurerReading();
            measurerReadingError2 = validateMeasurerReading2();
        }
        else{
            $("#measurerReadingErrorMessage").removeClass("d-block");
            $("#measurerReadingErrorMessage").addClass("d-none");
        }
    }
    let measurerIdError = validateMeasurerId();
    if (
        measurerIdError ||
        typeError ||
        imageError ||
        measurerReadingError ||
        measurerReadingError2
    )
        return true;
    return false;
}

function validateImage() {
    let invalid;
    let url = "api/config/isUploaded";
    $.post({
        url: url,
        async: false,
        statusCode: {
            200: (response) => {
                console.log(response);
                invalid = false;
                $("#imageErrorMessage").removeClass("d-block");
                $("#imageErrorMessage").addClass("d-none");
            },
            201: (response) => {
                console.log(response);
                invalid = true;
                $("#imageErrorMessage").removeClass("d-none");
                $("#imageErrorMessage").addClass("d-block");
            },
        },
    });
    return invalid;
}

function validateType() {
    let type = $("#type").val();
    if (type == null || type == "") {
        $("#typeErrorMessage").removeClass("d-none");
        $("#typeErrorMessage").addClass("d-block");
        $("#type").focus();
        return true;
    } else {
        $("#typeErrorMessage").removeClass("d-block");
        $("#typeErrorMessage").addClass("d-none");
        return false;
    }
}

function validateMeasurerId() {
    let measurerId = $("#measurerId").val();
    if (measurerId == null || measurerId == "") {
        $("#measurerIdErrorMessage").removeClass("d-none");
        $("#measurerIdErrorMessage").addClass("d-block");
        $("#measurerId").focus();
        return true;
    } else {
        $("#measurerIdErrorMessage").removeClass("d-block");
        $("#measurerIdErrorMessage").addClass("d-none");
        return false;
    }
}

function validateMeasurerReading() {
    let measurerReading = $("#measurerReading").val().trim();
    if (measurerReading == "") {
        $("#measurerReadingErrorMessage").removeClass("d-none");
        $("#measurerReadingErrorMessage").addClass("d-block");
        $("#measurerReading").focus();
        return true;
    } else {
        $("#measurerReadingErrorMessage").removeClass("d-block");
        $("#measurerReadingErrorMessage").addClass("d-none");
        return false;
    }
}

function validateMeasurerReading2() {
    let measurerIdError = validateMeasurerId();
    let invalid;
    if(measurerIdError) return;
    let measurerId = $("#measurerId").val();
    let measurerReading = $("#measurerReading").val().trim();
    let data = {
        measurerId: measurerId,
        measurerReading: measurerReading
    };
    let url = "api/reading/isValid";
    $.post({
        url: url,
        data: data,
        async: false,
        statusCode: {
            200: (response) => {
                console.log(response);
                $("#measurerReadingErrorMessage2").removeClass("d-block");
                $("#measurerReadingErrorMessage2").addClass("d-none");
                invalid = false;
            },
            201: (response) => {
                console.log(response);
                $("#measurerReadingErrorMessage2").removeClass("d-none");
                $("#measurerReadingErrorMessage2").addClass("d-block");
                $("#measurerReading").focus();
                invalid = true;
            },
        },
    });
    return invalid;
}

function fillSelectMeasurerId(){
    $('#measurerId').empty();
    $option = $('<option />', {
        text: "N° Medidor",
        value: "",
        readonly: true
    });
    $('#measurerId').prepend($option);
    let url = "api/measurer/read";
    $.post({
        url: url,
        async: false,
        statusCode: {
            200: (response) => {
                let measurers = response.measurers;
                measurers.forEach(measurer => {
                    $option = $('<option />', {
                        text: measurer.measurerNumber,
                        value: measurer.measurerId
                    });
                    $('#measurerId').prepend($option);
                });
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

function create() {
    let url = "api/reading/create";
    let data;
    let measurerId = $('#measurerId').val();
    let type = $('#type').val();
    let description = $('#description').val();

    if(type == "1"){
        let measurerReading = $('#measurerReading').val();
        data = {
            measurerId: measurerId,
            type: type,
            measurerReading: measurerReading,
            description: description
        };
    }
    else
        data = {
            measurerId: measurerId,
            type: type,
            description: description
        }

    let token = getToken();

    let headers = {
        Authorization: "Bearer " + token,
    };

    $.post({
        url: url,
        headers: headers,
        data: data,
        statusCode: {
            200: (response) => {
                Swal.fire({
                    type: "success",
                    title: "ÉXITO",
                    text: `Lectura registrada Exitosamente`,
                    confirmButtonText: `Aceptar`,
                }).then((response) => {
                    clean();
                    fillPendingReadings();
                });
            },
            500: (response) => {
                console.log(response);
                Swal.fire({
                    type: "error",
                    title: "ERROR",
                    text: "No se tiene registrado el precio a cobrar para esta fecha",
                    confirmButtonText: `Aceptar`,
                }).then((response) => {
                    clean();
                });
            },
            502: (response) => {
                console.log(response);
                Swal.fire({
                    type: "error",
                    title: "ERROR",
                    text: "El medidor no puede ser pronosticado sin lecturas previas",
                    confirmButtonText: `Aceptar`,
                }).then((response) => {
                    clean();
                });
            },
            503: (response) => {
                console.log(response);
                Swal.fire({
                    type: "error",
                    title: "ERROR",
                    text: "El medidor ya tiene registrada la lectura de este mes",
                    confirmButtonText: `Aceptar`,
                }).then((response) => {
                    clean();
                });
            },
        },
    });
}

function clean(){
    fillSelectMeasurerId();
    $('#measurerReading').val("");
    $('#description').val("")
}

function getToken(){
    let token;
    if (sessionStorage.token || localStorage.token) {
        if (sessionStorage.token) token = sessionStorage.token;
        else token = localStorage.token;
    }
    return token;
}
