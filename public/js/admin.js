fillClientsQuantity();
fillUsersQuantity();
fillMeasurersQuantity();

$(document).ready(function () {
    isLogged('users-module');
    fillLogs();
});

$('#searchBtn').on("click", (event) => {
    event.preventDefault();
    if (error()) return;
    updateIncomes();
});

function fillClientsQuantity(){
    let url = "api/client/quantity";
    $.post({
        url: url,
        success: (response) => {
            $('#clientsQuantity').html(response);
        },
    });
}

function fillUsersQuantity(){
    let url = "api/user/quantity";
    $.post({
        url: url,
        success: (response) => {
            $('#usersQuantity').html(response);
        },
    });
}

function fillMeasurersQuantity(){
    let url = "api/measurer/quantity";
    $.post({
        url: url,
        success: (response) => {
            $('#measurersQuantity').html(response);
        },
    });
}

function error() {
    let initialDateError = validateInitialDate();
    let finalDateError = validateFinalDate();

    if (
        initialDateError ||
        finalDateError
    )
        return true;
    return false;
}

function validateInitialDate(){
    let initialDate = $("#initialDate").val().trim();
    if (initialDate == "") {
        $("#initialDateErrorMessage").removeClass("d-none");
        $("#initialDateErrorMessage").addClass("d-block");
        return true;
    } else {
        $("#initialDateErrorMessage").removeClass("d-block");
        $("#initialDateErrorMessage").addClass("d-none");
        return false;
    }
}

function validateFinalDate(){
    let finalDate = $("#finalDate").val().trim();
    if (finalDate == "") {
        $("#finalDateErrorMessage").removeClass("d-none");
        $("#finalDateErrorMessage").addClass("d-block");
        return true;
    } else {
        $("#finalDateErrorMessage").removeClass("d-block");
        $("#finalDateErrorMessage").addClass("d-none");
        return false;
    }
}

function updateIncomes(){
    let initialDate = $('#initialDate').val();
    let finalDate = $('#finalDate').val();
    $('.initialDate').html(initialDate);
    $('.finalDate').html(finalDate);
    initialDate = getNumericDate(initialDate);
    finalDate = getNumericDate(finalDate);
    let data = {
        initialDate: initialDate,
        finalDate: finalDate
    }
    let url = "api/income/read";
    $.post({
        url: url,
        data: data,
        async: false,
        success: (response) => {
            fillIncomes();
        },
    });
}

function read() {
    let url = "api/log/read";
    $.post({
        url: url,
        async: false,
        success: (response) => {},
    });
}

function getNumericDate(date) {
    let dateParts = date.split(" ");
    let day = dateParts[0];
    let month = getNumericMonth(dateParts[1].substr(0, dateParts[1].length - 1));
    let year = dateParts[2];

    return `${year}/${month}/${day}`;
}

function getNumericMonth(month){
    let months = ['-','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
    return months.indexOf(month);
}
