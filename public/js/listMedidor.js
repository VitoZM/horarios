var actualMeasurerId;

$(document).ready(function () {
    isLogged("read-measurers");
    fillMeasurers();
});

function read() {
    let url = "api/measurer/read";
    $.post({
        url: url,
        async: false,
        success: (response) => {
            console.log(response);
        },
    });
}

function cleanErrors(){
    $("#clientIdErrorMessage").removeClass("d-block");
    $("#clientIdErrorMessage").addClass("d-none");
    $("#measurerNumberErrorMessage").removeClass("d-block");
    $("#measurerNumberErrorMessage").addClass("d-none");
    $("#measurerNumberErrorMessage2").removeClass("d-block");
    $("#measurerNumberErrorMessage2").addClass("d-none");
    $("#zoneErrorMessage").removeClass("d-block");
    $("#zoneErrorMessage").addClass("d-none");
    $("#installedAddressErrorMessage").removeClass("d-block");
    $("#installedAddressErrorMessage").addClass("d-none");
    $("#statusErrorMessage").removeClass("d-block");
    $("#statusErrorMessage").addClass("d-none");
    $("#dateErrorMessage").removeClass("d-block");
    $("#dateErrorMessage").addClass("d-none");
}

function fillEditModal(measurerId) {
    cleanErrors();
    fillSelectClientId();
    actualMeasurerId = measurerId;
    let measurer = getMeasurer(measurerId);
    fillClientId(measurer.clientId);
    $('#measurerNumber').val(measurer.measurerNumber);
    $("#zone").val(measurer.zone);
    $("#installedAddress").val(measurer.installedAddress);
    fillStatus(measurer.status);
    let date = new Date(measurer.registerDate);
    date.setDate(date.getDate()+1);
    let day = date.getDate();
    let month = getStringMonth(parseInt(date.getMonth())+1);
    let year = date.getFullYear();
    $("#registerDate").val(
        `${day} ${month}, ${year}`
    );
    $('#description').html(measurer.description);
}

function fillClientId(clientId){
    $("#clientId option").each(function() {
        if(this.value == clientId)
            this.selected = true;
    });
}

function getMeasurer(measurerId) {
    let measurer;
    let url = "api/measurer/findById";
    let data = { measurerId: measurerId };
    $.post({
        url: url,
        data: data,
        async: false,
        success: (response) => {
            measurer = response.measurer;
        },
    });
    return measurer;
}

function fillSelectClientId(){
    $('#clientId').empty();
    let url = "api/client/read";
    $.post({
        url: url,
        async: false,
        statusCode: {
            200: (response) => {
                clients = response.clients;
                clients.forEach(client => {
                    let option = $('<option />', {
                        text: client.clientNumber,
                        value: client.clientId
                    });
                    $('#clientId').prepend(option);
                });
            },
            500: (response) => {
                console.log(response);
            }
        },
    });
}

function fillStatus(status){
    let statusString = "";
    switch(status){
        case "1":
            statusString = "En Servicio";
            break;
        case "2":
            statusString = "En Corte";
            break;
        case "3":
            statusString = "Suspendido";
            break;
        case "4":
            statusString = "Dado de Baja";
            break;
    }
    $('#status').val(statusString);
}

/*function fillStatusSelect(status){
    $('#status').empty()
    let selected = false;
    if(status == "1")
        selected = true;
    let $option = $('<option />', {
        text: 'En Servicio',
        value: '1',
        selected: selected,
        "data-icon": "feather icon-check"
    });
    $('#status').prepend($option);
    selected = false;
    if(status == "2")
        selected = true;
    $option = $('<option />', {
        text: 'En Corte',
        value: '2',
        selected: selected,
        "data-icon": "feather icon-x-squarer"
    });
    $('#status').prepend($option);
    selected = false;
    if(status == "3")
        selected = true;
    $option = $('<option />', {
        text: 'Suspendido',
        value: '3',
        selected: selected,
        "data-icon": "feather icon-alert-octagon"
    });
    $('#status').prepend($option);
    selected = false;
    if(status == "4")
        selected = true;
    $option = $('<option />', {
        text: 'Dado de Baja',
        value: '4',
        selected: selected,
        "data-icon": "feather icon-alert-octagon"
    });
    $('#status').prepend($option);
}*/

$("#updateBtn").on("click", (event) => {
    event.preventDefault();
    if (error()) return;
    update();
    $("#editarMedidor").modal("hide");
});

function error() {
    let registerDateError = validateRegisterDate();
    let statusError = validateStatus();
    let installedAddresError = validateInstalledAddress();
    let zoneError = validateZone();
    let measurerNumberError = validateMeasurerNumber();
    let measurerNumberError2 = validateMeasurerNumber2();
    let clientIdError = validateClientId();

    if (
        clientIdError ||
        installedAddresError ||
        zoneError ||
        measurerNumberError ||
        measurerNumberError2 ||
        statusError ||
        registerDateError
    )
        return true;
    return false;
}

function validateRegisterDate(){
    let registerDate = $("#registerDate").val();
    if (registerDate == "") {
        $("#registerDateErrorMessage").removeClass("d-none");
        $("#registerDateErrorMessage").addClass("d-block");
        $("#registerDate").focus();
        return true;
    } else {
        $("#registerDateErrorMessage").removeClass("d-block");
        $("#registerDateErrorMessage").addClass("d-none");
        return false;
    }
}

function validateStatus() {
    let status = $("#status").val();
    if (status == "" || clientId == null) {
        $("#statusErrorMessage").removeClass("d-none");
        $("#statusErrorMessage").addClass("d-block");
        $("#status").focus();
        return true;
    } else {
        $("#statusErrorMessage").removeClass("d-block");
        $("#statusErrorMessage").addClass("d-none");
        return false;
    }
}

function validateInstalledAddress() {
    let installedAddress = $("#installedAddress").val().trim();
    if (installedAddress == "") {
        $("#installedAddressErrorMessage").removeClass("d-none");
        $("#installedAddressErrorMessage").addClass("d-block");
        $("#installedAddress").focus();
        return true;
    } else {
        $("#installedAddressErrorMessage").removeClass("d-block");
        $("#installedAddressErrorMessage").addClass("d-none");
        return false;
    }
}

function validateZone() {
    let zone = $("#zone").val().trim();
    if (zone == "") {
        $("#zoneErrorMessage").removeClass("d-none");
        $("#zoneErrorMessage").addClass("d-block");
        $("#zone").focus();
        return true;
    } else {
        $("#zoneErrorMessage").removeClass("d-block");
        $("#zoneErrorMessage").addClass("d-none");
        return false;
    }
}

function validateMeasurerNumber() {
    let measurerNumber = $("#measurerNumber").val().trim();
    if (measurerNumber == "") {
        $("#measurerNumberErrorMessage").removeClass("d-none");
        $("#measurerNumberErrorMessage").addClass("d-block");
        $("#measurerNumber").focus();
        return true;
    } else {
        $("#measurerNumberErrorMessage").removeClass("d-block");
        $("#measurerNumberErrorMessage").addClass("d-none");
        return false;
    }
}

function validateClientId() {
    let clientId = $("#clientId").val();
    if (clientId == "" || clientId == null) {
        $("#clientIdErrorMessage").removeClass("d-none");
        $("#clientIdErrorMessage").addClass("d-block");
        $("#clientId").focus();
        return true;
    } else {
        $("#clientIdErrorMessage").removeClass("d-block");
        $("#clientIdErrorMessage").addClass("d-none");
        return false;
    }
}

function validateMeasurerNumber2() {
    let invalid;
    let measurerNumber = $("#measurerNumber").val().trim();
    let url = "api/measurer/findByMeasurerNumberByMeasurerId";
    $.post({
        url: url,
        data: {
            measurerNumber: measurerNumber,
            measurerId: actualMeasurerId
        },
        async: false,
        statusCode: {
            200: (response) => {
                invalid = true;
                $("#measurerNumberErrorMessage2").removeClass("d-none");
                $("#measurerNumberErrorMessage2").addClass("d-block");
                $("#measurerNumber").focus();
            },
            500: (response) => {
                invalid = false;
                $("#measurerNumberErrorMessage2").removeClass("d-block");
                $("#measurerNumberErrorMessage2").addClass("d-none");
            },
        },
    });
    return invalid;
}

function update() {
    let measurerId = actualMeasurerId;
    let clientId = $("#clientId").val().trim();
    let measurerNumber = $("#measurerNumber").val().trim();
    let zone = format($("#zone").val());
    let installedAddress = format($("#installedAddress").val());
    //let status = $('#status').val();
    let registerDate = getNumericDate($("#registerDate").val());
    let description = format($("#description").val());

    let url = "api/measurer/update";
    $.post({
        url: url,
        data: {
            measurerId: measurerId,
            clientId: clientId,
            measurerNumber: measurerNumber,
            zone: zone,
            installedAddress: installedAddress,
            //status: status,
            registerDate: registerDate,
            description: description
        },
        statusCode: {
            200: (response) => {
                Swal.fire({
                    type: "success",
                    title: "EXITO",
                    text: `Medidor registrado Exitosamente`,
                    confirmButtonText: `Aceptar`,
                });
                fillMeasurers();
            },
            500: (response) => {
                console.log(response);
            }
        },
    });
}

function changeStatus(measurerId, status) {
    let url = "api/measurer/changeStatus";
    let data = {
        measurerId: measurerId,
        status: status
    };
    $.post({
        url: url,
        data: data,
        success: (response) => {
            window.location = "listMedidor";
        },
    });
}

$("#deleteBtn").on("click", (event) => {
    event.preventDefault();
    deleteClient(actualClientId);
});

function getStringMonth(number){
    let months = ['-','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
    return months[parseInt(number)];
}

function getNumericDate(date) {
    let dateParts = date.split(" ");
    let day = dateParts[0];
    let month = getNumericMonth(dateParts[1].substr(0, dateParts[1].length - 1));
    let year = dateParts[2];

    return `${year}/${month}/${day}`;
}

function getNumericMonth(month){
    let months = ['-','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
    return months.indexOf(month);
}


function format(string) {
    let result = string.trim().toLowerCase();
    result = result.replace(/\b\w/g, l => l.toUpperCase());
    return result;
}
