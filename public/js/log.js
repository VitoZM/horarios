var token;

function isLogged(permission) {
    if (sessionStorage.token || localStorage.token) {
        if (sessionStorage.token) token = sessionStorage.token;
        else token = localStorage.token;

        let user = me(token, permission);
        setUserName(user);
        setJob();
        setViews(user);
    } else window.location = "login";
}

function setViews(user) {
    let permissions = getAllPermissions();

    permissions.forEach((permission) => {
        showView(user, permission.name);
    });
}

function showView(user, permission) {
    let can = user.privileges.find((element) => element.name == permission);
    if (can != undefined) {
        $("." + permission).removeClass("d-none");
        $("." + permission).addClass("d-block");
    }
}

function getAllPermissions(){
    let permissions;
    let url = "api/permission/read";
    $.post({
        url: url,
        async: false,
        statusCode: {
            200: (response) => {
                //console.log(response);
                permissions = JSON.parse(response);
            }
        },
        succes: (result) => {
            console.log(result);
        },
    });
    return permissions;
}

function setUserName(user) {
    $("#userName").html(user.userName);
}

function setJob() {
    let profile = JSON.parse(localStorage.profile);
    $("#job").html(profile.job);
}

function me(token, permission) {
    let user;
    let url = "api/auth/me";
    let headers = {
        Authorization: "Bearer " + token,
    };
    let data = {
        token: token,
        permission: permission,
    };
    $.post({
        url: url,
        headers: headers,
        data: data,
        async: false,
        statusCode: {
            200: (response) => {
                //console.log(response);
                user = response;
            },
            400: (response) => {
                console.log(response);
                window.location = "admin";
            },
            401: (response) => {
                console.log(response);
                localStorage.removeItem("token");
                sessionStorage.removeItem("token");
                window.location = "login";
            },
        },
        succes: (result) => {
            console.log(result);
        },
    });
    return user;
}

$("#logOutBtn").on("click", (event) => {
    event.preventDefault();
    storageClean();

    let url = "api/log/update";
    let headers = {
        Authorization: "Bearer " + token,
    };
    let data = {
        token: token,
    };
    $.post({
        url: url,
        headers: headers,
        data: data,
        statusCode: {
            200: (response) => {
                console.log(response);
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
    window.location = "login";
});

function storageClean() {
    localStorage.removeItem("token");
    localStorage.removeItem("profile");
    sessionStorage.removeItem("token");
}
