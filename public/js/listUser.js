var actualUserId;

$(document).ready(function () {
    isLogged("read-users");
    fillUsers();
});

function read() {
    let url = "api/user/read";
    $.post({
        url: url,
        async: false,
        success: (response) => {
            console.log(response);
        },
    });
}

function deleteUser(userId) {
    let url = "api/user/delete";
    let data = {
        userId: userId,
    };
    $.post({
        url: url,
        data: data,
        statusCode: {
            200: (response) => {
                console.log(response);
                Swal.fire({
                    type: "success",
                    title: "EXITO",
                    text: `Usuario eliminado Exitosamente`,
                    confirmButtonText: `Aceptar`,
                }).then((response) => {
                    fillUsers();
                });
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

$("#deleteBtn").on("click", (event) => {
    event.preventDefault();
    deleteUser(actualUserId);
});

function cleanErrors(){
    $("#entryDateErrorMessage").removeClass("d-block");
    $("#entryDateErrorMessage").addClass("d-none");
    $("#jobErrorMessage").removeClass("d-block");
    $("#jobErrorMessage").addClass("d-none");
    $("#birthDayErrorMessage").removeClass("d-block");
    $("#birthDayErrorMessage").addClass("d-none");
    $("#sexErrorMessage").removeClass("d-block");
    $("#sexErrorMessage").addClass("d-none");
    $("#nameErrorMessage").removeClass("d-block");
    $("#nameErrorMessage").addClass("d-none");
    $("#lastNameErrorMessage").removeClass("d-block");
    $("#lastNameErrorMessage").addClass("d-none");
}

function fillEditModal(userId, userName) {
    cleanErrors();
    actualUserId = userId;
    let profile = getProfile(userId);
    $("#formUserName").val(userName);
    $("#name").val(profile.name);
    $("#lastName").val(profile.lastName);
    fillSexSelect(profile.sex);
    fillJobSelect(profile.job);
    let birthDayParts = profile.birthDay.split("-");
    let day = birthDayParts[2];
    let month = getStringMonth(birthDayParts[1]);
    let year = birthDayParts[0];
    $("#birthDay").val(
        `${day} ${month}, ${year}`
    );
    let entryDateParts = profile.entryDate.split(" ")[0].split("-");
    day = entryDateParts[2];
    month = getStringMonth(entryDateParts[1]);
    year = entryDateParts[0];
    $("#entryDate").val(
        `${day} ${month}, ${year}`
    );
}

function getStringMonth(number){
    let months = ['-','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
    return months[parseInt(number)];
}

function fillJobSelect(job){
    $('#formJob').empty()
    let selected = false;
    if(job == "Administrador")
        selected = true;
    let $option = $('<option />', {
        text: 'Administrador',
        value: '1',
        selected: selected,
        "data-icon": "feather icon-server"
    });
    $('#formJob').prepend($option);
    selected = false;
    if(job == "Secretaria/o")
        selected = true;
    $option = $('<option />', {
        text: 'Secretaria/o',
        value: '2',
        selected: selected,
        "data-icon": "feather icon-monitor"
    });
    $('#formJob').prepend($option);
    selected = false;
    if(job == "Lector")
        selected = true;
    $option = $('<option />', {
        text: 'Lector',
        value: '3',
        selected: selected,
        "data-icon": "feather icon-file-text"
    });
    $('#formJob').prepend($option);
}

function fillSexSelect(sex){
    $('#sex').empty()
    let selected = false;
    if(sex == "Masculino")
        selected = true;
    let $option = $('<option />', {
        text: 'Masculino',
        value: 'Masculino',
        selected: selected,
        "data-icon": "feather icon-user"
    });
    $('#sex').prepend($option);
    selected = false;
    if(sex == "Femenino")
        selected = true;
    $option = $('<option />', {
        text: 'Femenino',
        value: 'Femenino',
        selected: selected,
        "data-icon": "feather icon-user"
    });
    $('#sex').prepend($option);
}

$("#saveBtn").on("click", (event) => {
    event.preventDefault();
    if (error()) return;
    update();
    $("#editarUsuario").modal("hide");
});

function error() {
    let entryDateError = validateEntryDate();
    let jobError = validateJob();
    let birthDayError = validateBirthDay();
    let sexError = validateSex();
    let lastNameError = validateLastName();
    let nameError = validateName();

    if (nameError || lastNameError || sexError || birthDayError || jobError || entryDateError) return true;
    return false;
}

function validateEntryDate() {
    let entryDate = $("#entryDate").val().trim();
    if (entryDate == "") {
        $("#entryDateErrorMessage").removeClass("d-none");
        $("#entryDateErrorMessage").addClass("d-block");
        return true;
    } else {
        $("#entryDateErrorMessage").removeClass("d-block");
        $("#entryDateErrorMessage").addClass("d-none");
        return false;
    }
}

function validateJob(){
    let job = $("#formJob").val();
    if (job == null || job == "0" | job == "") {
        $("#jobErrorMessage").removeClass("d-none");
        $("#jobErrorMessage").addClass("d-block");
        $("#formJob").focus();
        return true;
    } else {
        $("#jobErrorMessage").removeClass("d-block");
        $("#jobErrorMessage").addClass("d-none");
        return false;
    }
}

function validateBirthDay() {
    let birthDay = $("#birthDay").val().trim();
    if (birthDay == "") {
        $("#birthDayErrorMessage").removeClass("d-none");
        $("#birthDayErrorMessage").addClass("d-block");
        return true;
    } else {
        $("#birthDayErrorMessage").removeClass("d-block");
        $("#birthDayErrorMessage").addClass("d-none");
        return false;
    }
}

function validateSex(){
    let sex = $("#sex").val();
    if (sex == null || sex == "0" | sex == "") {
        $("#sexErrorMessage").removeClass("d-none");
        $("#sexErrorMessage").addClass("d-block");
        $("#sex").focus();
        return true;
    } else {
        $("#sexErrorMessage").removeClass("d-block");
        $("#sexErrorMessage").addClass("d-none");
        return false;
    }
}

function validateName(){
    let name = $("#name").val().trim();
    if (name == "") {
        $("#nameErrorMessage").removeClass("d-none");
        $("#nameErrorMessage").addClass("d-block");
        $("#name").focus();
        return true;
    } else {
        $("#nameErrorMessage").removeClass("d-block");
        $("#nameErrorMessage").addClass("d-none");
        return false;
    }
}

function validateLastName(){
    let name = $("#lastName").val().trim();
    if (name == "") {
        $("#lastNameErrorMessage").removeClass("d-none");
        $("#lastNameErrorMessage").addClass("d-block");
        $("#lastName").focus();
        return true;
    } else {
        $("#lastNameErrorMessage").removeClass("d-block");
        $("#lastNameErrorMessage").addClass("d-none");
        return false;
    }
}

function validatePassword() {
    let password = $("#password").val().trim();
    if (password == "") {
        $("#passwordErrorMessage").removeClass("d-none");
        $("#passwordErrorMessage").addClass("d-block");
        $("#password").focus();
        return true;
    } else {
        $("#passwordErrorMessage").removeClass("d-block");
        $("#passwordErrorMessage").addClass("d-none");
        return false;
    }
}

function update(){
    let name = $("#name").val();
    let lastName = $("#lastName").val();
    let sex = $("#sex").val();
    let birthDay = getNumericDate($("#birthDay").val()); //Envez de edad
    let job = $("#formJob").val();
    let roleId = job;
    switch (job) {
        case "1":
            job = "Administrador";
            break;
        case "2":
            job = "Secretaria/o";
            break;
        case "3":
            job = "Lector";
    }
    let entryDate = getNumericDate($("#entryDate").val());

    let url = "api/profile/update";
    $.post({
        url: url,
        data: {
            userId: actualUserId,
            name: name,
            lastName: lastName,
            sex: sex,
            birthDay: birthDay,
            job: job,
            entryDate: entryDate,
            roleId: roleId,
        },
        statusCode: {
            200: (response) => {
                console.log(response);
                Swal.fire({
                    type: "success",
                    title: "EXITO",
                    text: `Usuario editado Exitosamente`,
                    confirmButtonText: `Aceptar`,
                }).then((response) => {
                    fillUsers();
                });
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

function getProfile(userId) {
    let profile;
    let url = "api/profile/getProfile";
    let data = { userId: userId };
    $.post({
        url: url,
        data: data,
        async: false,
        success: (response) => {
            profile = response;
        },
    });
    return profile;
}

function getNumericDate(date) {
    let dateParts = date.split(" ");
    let day = dateParts[0];
    let month = getNumericMonth(dateParts[1].substr(0, dateParts[1].length - 1));
    let year = dateParts[2];

    return `${year}/${month}/${day}`;
}

function getNumericMonth(month){
    let months = ['-','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
    return months.indexOf(month);
}
