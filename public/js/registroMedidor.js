
isLogged('create-measurers');

fillSelectClientId();
fillClientId();

$("#createBtn").on("click", (event) => {
    event.preventDefault();
    if (error()) return;
    create();
});

function error() {
    let registerDateError = validateRegisterDate();
    let installedAddresError = validateInstalledAddress();
    let zoneError = validateZone();
    let measurerNumberError = validateMeasurerNumber();
    let measurerNumberError2 = validateMeasurerNumber2();
    let clientIdError = validateClientId();

    if (
        clientIdError ||
        installedAddresError ||
        zoneError ||
        measurerNumberError ||
        measurerNumberError2 ||
        registerDateError
    )
        return true;
    return false;
}

function validateRegisterDate(){
    let registerDate = $("#registerDate").val().trim();
    if (registerDate == "") {
        $("#registerDateErrorMessage").removeClass("d-none");
        $("#registerDateErrorMessage").addClass("d-block");
        $("#registerDate").focus();
        return true;
    } else {
        $("#registerDateErrorMessage").removeClass("d-block");
        $("#registerDateErrorMessage").addClass("d-none");
        return false;
    }
}

function validateInstalledAddress() {
    let installedAddress = $("#installedAddress").val().trim();
    if (installedAddress == "") {
        $("#installedAddressErrorMessage").removeClass("d-none");
        $("#installedAddressErrorMessage").addClass("d-block");
        $("#installedAddress").focus();
        return true;
    } else {
        $("#installedAddressErrorMessage").removeClass("d-block");
        $("#installedAddressErrorMessage").addClass("d-none");
        return false;
    }
}

function validateZone() {
    let zone = $("#zone").val().trim();
    if (zone == "") {
        $("#zoneErrorMessage").removeClass("d-none");
        $("#zoneErrorMessage").addClass("d-block");
        $("#zone").focus();
        return true;
    } else {
        $("#zoneErrorMessage").removeClass("d-block");
        $("#zoneErrorMessage").addClass("d-none");
        return false;
    }
}

function validateMeasurerNumber() {
    let measurerNumber = $("#measurerNumber").val().trim();
    if (measurerNumber == "") {
        $("#measurerNumberErrorMessage").removeClass("d-none");
        $("#measurerNumberErrorMessage").addClass("d-block");
        $("#measurerNumber").focus();
        return true;
    } else {
        $("#measurerNumberErrorMessage").removeClass("d-block");
        $("#measurerNumberErrorMessage").addClass("d-none");
        return false;
    }
}

function validateMeasurerNumber2() {
    let invalid;
    let measurerNumber = $("#measurerNumber").val().trim();
    let url = "api/measurer/findByMeasurerNumber";
    $.post({
        url: url,
        data: {
            measurerNumber: measurerNumber,
        },
        async: false,
        statusCode: {
            200: (response) => {
                invalid = true;
                $("#measurerNumberErrorMessage2").removeClass("d-none");
                $("#measurerNumberErrorMessage2").addClass("d-block");
                $("#measurerNumber").focus();
            },
            500: (response) => {
                invalid = false;
                $("#measurerNumberErrorMessage2").removeClass("d-block");
                $("#measurerNumberErrorMessage2").addClass("d-none");
            },
        },
    });
    return invalid;
}

function validateClientId() {
    let clientId = $("#clientId").val();
    if (clientId == "" || clientId == null) {
        $("#clientIdErrorMessage").removeClass("d-none");
        $("#clientIdErrorMessage").addClass("d-block");
        $("#clientId").focus();
        return true;
    } else {
        $("#clientIdErrorMessage").removeClass("d-block");
        $("#clientIdErrorMessage").addClass("d-none");
        return false;
    }
}

function fillSelectClientId(){
    let url = "api/client/read";
    $.post({
        url: url,
        async: false,
        statusCode: {
            200: (response) => {
                clients = response.clients;
                clients.forEach(client => {
                    $option = $('<option />', {
                        text: client.clientNumber,
                        value: client.clientId
                    });
                    $('#clientId').prepend($option);
                });
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

function fillClientId(){
    if(localStorage.client){
        client = JSON.parse(localStorage.client);
        $("#clientId option").each(function() {
            if(this.value == client.clientId)
                this.selected = true;
        });
    }
}

function create() {
    let clientId = $("#clientId").val().trim();
    let measurerNumber = $("#measurerNumber").val().trim();
    let zone = $("#zone").val().trim();
    let installedAddress = format($("#installedAddress").val());
    let registerDate = getNumericDate($('#registerDate').val());
    let description = format($("#description").val());

    let url = "api/measurer/create";
    $.post({
        url: url,
        data: {
            clientId: clientId,
            measurerNumber: measurerNumber,
            zone: zone,
            installedAddress: installedAddress,
            registerDate: registerDate,
            description: description
        },
        statusCode: {
            200: (response) => {
                Swal.fire({
                    type: "success",
                    title: "EXITO",
                    text: `Medidor registrado Exitosamente`,
                    confirmButtonText: `Aceptar`,
                });
                clean();
            },
            500: (response) => {
                console.log(response);
            }
        },
    });
}

function clean() {
    $("#clientId").val("");
    $("#measurerNumber").val("");
    $("#zone").val("");
    $("#installedAddress").val("");
    $("#registerDate").val("");
    $("#description").val("");
}

function getNumericDate(date) {
    let dateParts = date.split(" ");
    let day = dateParts[0];
    let month = getNumericMonth(dateParts[1].substr(0, dateParts[1].length - 1));
    let year = dateParts[2];

    return `${year}/${month}/${day}`;
}

function getNumericMonth(month){
    let months = ['-','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
    return months.indexOf(month);
}

function format(string) {
    let result = string.trim().toLowerCase();
    result = result.replace(/\b\w/g, l => l.toUpperCase());
    return result;
}
