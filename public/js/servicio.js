var actualMeasurerId;

$(document).ready(function () {
    isLogged('update-measurers');
    fillMeasurersOnOutage();
    fillMeasurers();
});

$("#outageBtn").on("click", () => {
    outage();
});

$("#serviceBtn").on("click", () => {
    service();
});

function read() {
    let url = "api/measurer/readOnServiceToOutage";
    $.post({
        url: url,
        async: false,
        success: (response) => {

        },
    });
}

function read2() {
    let url = "api/measurer/readOnOutageToService";
    $.post({
        url: url,
        async: false,
        success: (response) => {

        },
    });
}

function setActualMeasurerId(measurerId){
    actualMeasurerId = measurerId;
}

function outage(){
    let data = { measurerId: actualMeasurerId };
    let url = "api/measurer/outage";
    $.post({
        url: url,
        data: data,
        async: false,
        success: (response) => {
            fillMeasurers();
        },
    });
}

function service(){
    let data = { measurerId: actualMeasurerId };
    let url = "api/measurer/service";
    $.post({
        url: url,
        data: data,
        async: false,
        success: (response) => {
            fillMeasurersOnOutage();
        },
    });
}
