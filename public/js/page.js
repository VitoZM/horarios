$("#searchBtn").on("click", function (event) {
    event.preventDefault();
    if (error()) return;
    fillTable();
});

function error() {
    let measurerNumberError = validateMeasurerNumber();
    let clientNumberError = validateClientNumber();
    if (measurerNumberError || clientNumberError) return true;
    return false;
}

function validateMeasurerNumber() {
    let measurerNumber = $("#measurerNumber").val();
    if (measurerNumber == null || measurerNumber == "") {
        $("#measurerNumberErrorMessage").removeClass("d-none");
        $("#measurerNumberErrorMessage").addClass("d-block");
        return true;
    } else {
        $("#measurerNumberErrorMessage").removeClass("d-block");
        $("#measurerNumberErrorMessage").addClass("d-none");
        return false;
    }
}

function validateClientNumber() {
    let clientNumber = $("#clientNumber").val();
    if (clientNumber == "" || clientNumber == null) {
        $("#clientNumberErrorMessage").removeClass("d-none");
        $("#clientNumberErrorMessage").addClass("d-block");
        return true;
    } else {
        $("#clientNumberErrorMessage").removeClass("d-block");
        $("#clientNumberErrorMessage").addClass("d-none");
        return false;
    }
}

function fillTable() {
    let billings = getBillings();
    if (billings.length != "0")
        fillBillings();
    else
        document.getElementById("myGrid").innerHTML = "<h1>No Tiene Facturas Por Pagar</h1>";
}

function fillHeader(client) {
    if(client != null){
        $(".clientNumber").html(client.clientNumber);
        $(".measurerNumber").html(client.measurerNumber);
        $("#clientName").html(client.lastName + " " + client.name);
    }
    else{
        Swal.fire({
            type: "error",
            title: "ERROR",
            text: `El número de cliente no esta asociado al número de medidor`,
            confirmButtonText: `Aceptar`,
        }).then((response) => {
            window.location = '#head';
        });
        $("#clientNumber").html("-");
        $("#measurerNumber").html("-");
        $("#clientName").html("-");
    }
}

function getBillings() {
    let billings;
    let clientNumber = $("#clientNumber").val();
    let measurerNumber = $("#measurerNumber").val();
    let url = "api/billing/getAllBillings";
    let data = {
        clientNumber: clientNumber,
        measurerNumber: measurerNumber,
    };
    $.post({
        url: url,
        data: data,
        async: false,
        statusCode: {
            200: (response) => {
                console.log(response);
                cleanErrors();
                window.location = "#basic-examples";
                fillHeader(response.client);
                billings = response.billings;
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
    return billings;
}

function cleanErrors() {
    $("#clientNumberErrorMessage").removeClass("d-block");
    $("#clientNumberErrorMessage").addClass("d-none");
    $("#measurerNumberErrorMessage").removeClass("d-block");
    $("#measurerNumberErrorMessage").addClass("d-none");
}
