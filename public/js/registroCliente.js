localStorage.removeItem('client');

isLogged('create-clients');
fillRegistrationPrice();


function fillRegistrationPrice(){
    let registrationPrice;
    let url = "api/registrationPrice/read";
    $.post({
        url: url,
        async: false,
        statusCode: {
            200: (response) => {
                console.log(response);
                $("#payment").val(response.registrationPrice.price);
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

$("#createBtn").on("click", (event) => {
    event.preventDefault();
    if (error()) return;
    create();
});

function error() {
    let cellphoneError = validateCellphone();
    let birthDayError = validateBirthDay();
    let sexError = validateSex();
    let addressNumberError = validateAddressNumber();
    let addressError = validateAddress();
    let lastNameError = validateLastName();
    let nameError = validateName();
    let ciError = validateCi();
    let ciError2 = validateCi2();
    let clientNumberError2 = validateClientNumber2();
    let clientNumberError = validateClientNumber();

    if (
        clientNumberError ||
        clientNumberError2 ||
        ciError ||
        ciError2 ||
        lastNameError ||
        nameError ||
        addressError ||
        addressNumberError ||
        birthDayError ||
        sexError ||
        cellphoneError
    )
        return true;
    return false;
}

function validateCellphone() {
    let cellphone = $("#cellphone").val().trim();
    if (cellphone == "") {
        $("#cellphoneErrorMessage").removeClass("d-none");
        $("#cellphoneErrorMessage").addClass("d-block");
        $("#cellphone").focus();
        return true;
    } else {
        $("#cellphoneErrorMessage").removeClass("d-block");
        $("#cellphoneErrorMessage").addClass("d-none");
        return false;
    }
}

function validateCi() {
    let ci = $("#ci").val().trim();
    if (ci == "") {
        $("#ciErrorMessage").removeClass("d-none");
        $("#ciErrorMessage").addClass("d-block");
        $("#ci").focus();
        return true;
    } else {
        $("#ciErrorMessage").removeClass("d-block");
        $("#ciErrorMessage").addClass("d-none");
        return false;
    }
}

function validateAddress() {
    let address = $("#address").val().trim();
    if (address == "") {
        $("#addressErrorMessage").removeClass("d-none");
        $("#addressErrorMessage").addClass("d-block");
        $("#address").focus();
        return true;
    } else {
        $("#addressErrorMessage").removeClass("d-block");
        $("#addressErrorMessage").addClass("d-none");
        return false;
    }
}

function validateAddressNumber() {
    let addressNumber = $("#addressNumber").val().trim();
    if (addressNumber == "") {
        $("#addressNumberErrorMessage").removeClass("d-none");
        $("#addressNumberErrorMessage").addClass("d-block");
        $("#addressNumber").focus();
        return true;
    } else {
        $("#addressNumberErrorMessage").removeClass("d-block");
        $("#addressNumberErrorMessage").addClass("d-none");
        return false;
    }
}

function validateCi2() {
    let invalid;
    let ci = $("#ci").val().trim();
    let url = "api/client/findByCi";
    $.post({
        url: url,
        data: {
            ci: ci,
        },
        async: false,
        statusCode: {
            200: (response) => {
                invalid = true;
                $("#ciErrorMessage2").removeClass("d-none");
                $("#ciErrorMessage2").addClass("d-block");
                $("#ci").focus();
            },
            500: (response) => {
                invalid = false;
                $("#ciErrorMessage2").removeClass("d-block");
                $("#ciErrorMessage2").addClass("d-none");
            },
        },
    });
    return invalid;
}

function validateBirthDay() {
    let birthDay = $("#birthDay").val().trim();
    if (birthDay == "") {
        $("#birthDayErrorMessage").removeClass("d-none");
        $("#birthDayErrorMessage").addClass("d-block");
        return true;
    } else {
        $("#birthDayErrorMessage").removeClass("d-block");
        $("#birthDayErrorMessage").addClass("d-none");
        return false;
    }
}

function validateSex() {
    let sex = $("#sex").val();
    if (sex == null || (sex == "0") | (sex == "")) {
        $("#sexErrorMessage").removeClass("d-none");
        $("#sexErrorMessage").addClass("d-block");
        $("#sex").focus();
        return true;
    } else {
        $("#sexErrorMessage").removeClass("d-block");
        $("#sexErrorMessage").addClass("d-none");
        return false;
    }
}

function validateName() {
    let name = $("#name").val().trim();
    if (name == "") {
        $("#nameErrorMessage").removeClass("d-none");
        $("#nameErrorMessage").addClass("d-block");
        $("#name").focus();
        return true;
    } else {
        $("#nameErrorMessage").removeClass("d-block");
        $("#nameErrorMessage").addClass("d-none");
        return false;
    }
}

function validateLastName() {
    let name = $("#lastName").val().trim();
    if (name == "") {
        $("#lastNameErrorMessage").removeClass("d-none");
        $("#lastNameErrorMessage").addClass("d-block");
        $("#lastName").focus();
        return true;
    } else {
        $("#lastNameErrorMessage").removeClass("d-block");
        $("#lastNameErrorMessage").addClass("d-none");
        return false;
    }
}

function validateClientNumber2() {
    let invalid;
    let clientNumber = $("#clientNumber").val().trim();
    let url = "api/client/findByClientNumber";
    $.post({
        url: url,
        data: {
            clientNumber: clientNumber,
        },
        async: false,
        statusCode: {
            200: (response) => {
                invalid = true;
                $("#clientNumberErrorMessage2").removeClass("d-none");
                $("#clientNumberErrorMessage2").addClass("d-block");
                $("#clientNumber").focus();
            },
            500: (response) => {
                invalid = false;
                $("#clientNumberErrorMessage2").removeClass("d-block");
                $("#clientNumberErrorMessage2").addClass("d-none");
            },
        },
    });
    return invalid;
}

function validateClientNumber() {
    let clientNumber = $("#clientNumber").val().trim();
    if (clientNumber == "") {
        $("#clientNumberErrorMessage").removeClass("d-none");
        $("#clientNumberErrorMessage").addClass("d-block");
        $("#clientNumber").focus();
        return true;
    } else {
        $("#clientNumberErrorMessage").removeClass("d-block");
        $("#clientNumberErrorMessage").addClass("d-none");
        return false;
    }
}

function create() {
    let clientNumber = $("#clientNumber").val().trim();
    let ci = $("#ci").val().trim();
    let name = format($("#name").val());
    let lastName = format($("#lastName").val());
    let address = format($("#address").val());
    let addressNumber = $('#addressNumber').val().trim();
    let sex = $('#sex').val();
    let birthDay = getNumericDate($("#birthDay").val());
    let cellphone = $("#cellphone").val().trim();

    let url = "api/client/create";
    $.post({
        url: url,
        data: {
            clientNumber: clientNumber,
            ci: ci,
            name: name,
            lastName: lastName,
            address: address,
            addressNumber: addressNumber,
            sex: sex,
            birthDay: birthDay,
            cellphone: cellphone
        },
        statusCode: {
            200: (response) => {
                localStorage.client = JSON.stringify(response.client);
                Swal.fire({
                    type: "success",
                    title: "EXITO",
                    text: `Usuario editado Exitosamente`,
                    confirmButtonText: `Aceptar`,
                }).then((response) => {
                    window.location = "registroMedidor";
                });
            },
            500: (response) => {
                console.log(response);
            }
        },
    });
}

function clean() {
    $("#clientNumber").val("");
    $("#ci").val("");
    $("#name").val("");
    $("#lastName").val("");
    $("#address").val("");
    $("#addressNumber").val("");
    $("#birthDay").val("");
    $("#cellphone").val("");
    fillRegistrationPrice();
}

function getNumericDate(date) {
    let dateParts = date.split(" ");
    let day = dateParts[0];
    let month = getNumericMonth(dateParts[1].substr(0, dateParts[1].length - 1));
    let year = dateParts[2];

    return `${year}/${month}/${day}`;
}

function getNumericMonth(month){
    let months = ['-','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
    return months.indexOf(month);
}

function format(string) {
    let result = string.trim().toLowerCase();
    result = result.replace(/\b\w/g, l => l.toUpperCase());
    return result;
}
