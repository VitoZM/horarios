var actualDiscountId;

$(document).ready(function () {
    isLogged("create-discount");
    fillDiscounts();
    fillMinimumAge();
});

function read() {
    let url = "api/discount/read";
    $.post({
        url: url,
        async: false,
        success: (response) => {},
    });
}

function fillEditModal(discountId){
    actualDiscountId = discountId;
    let discount = getDiscount(discountId);

    $('#modalMinimumAge').val(discount.minimumAge)
    $('#modalMaximumAge').val(discount.maximumAge)
    $('#modalDiscountPercentage').val(discount.discountPercentage);
}

function getStringDate(date){
    let dateParts = date.split("-");
    let day = dateParts[2];
    let month = getStringMonth(dateParts[1]);
    let year = dateParts[0];

    return `${day} ${month}, ${year}`;
}

function getDiscount(discountId) {
    let discount;

    let url = "api/discount/findById";
    let data = { discountId: discountId };
    $.post({
        url: url,
        data: data,
        async: false,
        success: (response) => {
            console.log(response);
            discount = response.discount;
        },
    });

    return discount;
}

function fillMinimumAge(){
    let url = "api/discount/getMinimumAge";
    $.post({
        url: url,
        async: false,
        statusCode: {
            200: (response) => {
                $("#minimumAge").val(response);
            },
            500: (response) => {
                console.log(response);
            }
        },
    });
}

function getStringMonth(number){
    let months = ['-','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
    return months[parseInt(number)];
}

$("#createBtn").on("click", (event) => {
    event.preventDefault();
    if (error()) return;
    create();
});

$('#updateBtn').on("click", (event) => {
    event.preventDefault();
    if(updateError()) return;
    update();
});

function updateError(){
    let discountPercentageError = validateModalDiscountPercentage();

    if(discountPercentageError)
        return true;
    return false;
}

function error() {
    let discountPercentageError = validateDiscountPercentage();
    let maximumAgeError = validateMaximumAge();
    let maximumAgeError2 = validateMaximumAge2();

    if (
        maximumAgeError ||
        maximumAgeError2 ||
        discountPercentageError
    )
        return true;
    return false;
}

function update() {
    let discountId = actualDiscountId;
    let discountPercentage = $('#modalDiscountPercentage').val().trim();

    let url = "api/discount/update";
    let data = {
        discountId: discountId,
        discountPercentage: discountPercentage
    };

    $.post({
        url: url,
        data: data,
        statusCode: {
            200: (response) => {
                console.log(response);
                Swal.fire({
                    type: "success",
                    title: "EXITO",
                    text: `Descuento editado Exitosamente`,
                    confirmButtonText: `Aceptar`,
                }).then((response) => {
                    fillDiscounts();
                    //window.location = "listCliente";
                });
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

function validateMaximumAge() {
    let maximumAge = $("#maximumAge").val().trim();
    if (maximumAge == "") {
        $("#maximumAgeErrorMessage").removeClass("d-none");
        $("#maximumAgeErrorMessage").addClass("d-block");
        return true;
    } else {
        $("#maximumAgeErrorMessage").removeClass("d-block");
        $("#maximumAgeErrorMessage").addClass("d-none");
        return false;
    }
}

function validateMaximumAge2() {
    let invalid;
    let maximumAge = $("#maximumAge").val();
    let url = "api/discount/validateMaximumAge";
    data = {
        maximumAge: maximumAge,
    };
    $.post({
        url: url,
        data: data,
        async: false,
        statusCode: {
            201: (response) => {
                invalid = true;
                $("#maximumAgeErrorMessage2").removeClass("d-none");
                $("#maximumAgeErrorMessage2").addClass("d-block");
            },
            200: (response) => {
                invalid = false;
                $("#maximumAgeErrorMessage2").removeClass("d-block");
                $("#maximumAgeErrorMessage2").addClass("d-none");
            },
        },
    });
    return invalid;
}

function validateDiscountPercentage() {
    let discountPercentage = $("#discountPercentage").val().trim();
    if (discountPercentage == "") {
        $("#discountPercentageErrorMessage").removeClass("d-none");
        $("#discountPercentageErrorMessage").addClass("d-block");
        $('#discountPercentage').focus();
        return true;
    } else {
        $("#discountPercentageErrorMessage").removeClass("d-block");
        $("#discountPercentageErrorMessage").addClass("d-none");
        return false;
    }
}

function validateModalDiscountPercentage() {
    let discountPercentage = $("#modalDiscountPercentage").val().trim();
    if (discountPercentage == "") {
        $("#modalDiscountPercentageErrorMessage").removeClass("d-none");
        $("#modalDiscountPercentageErrorMessage").addClass("d-block");
        $('#modalDiscountPercentageErrorMessage').focus();
        return true;
    } else {
        $("#modalDiscountPercentageErrorMessage").removeClass("d-block");
        $("#modalDiscountPercentageErrorMessage").addClass("d-none");
        return false;
    }
}

function create() {
    let maximumAge = $("#maximumAge").val();
    let discountPercentage = $("#discountPercentage").val().trim();
    let data = {
        maximumAge: maximumAge,
        discountPercentage: discountPercentage
    };
    let url = "api/discount/create";
    $.post({
        url: url,
        data: data,
        statusCode: {
            200: (response) => {
                console.log(response);
                Swal.fire({
                    type: "success",
                    title: "EXITO",
                    text: `Descuento creado Exitosamente`,
                    confirmButtonText: `Aceptar`,
                }).then((response) => {
                    fillMinimumAge();
                    fillDiscounts();
                    clean();
                });
            },
            500: (response) => {
                console.log(response);
            }
        },
    });
}

function clean(){
    $('#maximumAge').val("");
    $('#discountPercentage').val("");
}

function getNumericDate(date) {
    let dateParts = date.split(" ");
    let day = dateParts[0];
    let month = getNumericMonth(dateParts[1].substr(0, dateParts[1].length - 1));
    let year = dateParts[2];

    return `${year}/${month}/${day}`;
}

function getNumericMonth(month){
    let months = ['-','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
    return months.indexOf(month);
}
