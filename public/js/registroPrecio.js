var actualPricePerMeterId;
$(document).ready(function () {
    isLogged('create-price');
    fillInitialDate();
    fillPrices();
});

function read() {
    let url = "api/pricePerMeter/read";
    $.post({
        url: url,
        async: false,
        success: (response) => {},
    });
}

function fillEditModal(pricePerMeterId){
    actualPricePerMeterId = pricePerMeterId;
    let pricePerMeter = getPricePerMeter(pricePerMeterId);

    $('#modalInitialDate').val(getStringDate(pricePerMeter.initialDate))
    $('#modalFinalDate').val(getStringDate(pricePerMeter.finalDate))
    $('#modalPrice').val(pricePerMeter.price);
}

function getStringDate(date){
    let dateParts = date.split("-");
    let day = dateParts[2];
    let month = getStringMonth(dateParts[1]);
    let year = dateParts[0];

    return `${day} ${month}, ${year}`;
}

function getPricePerMeter(pricePerMeterId) {
    let pricePerMeter;

    let url = "api/pricePerMeter/findById";
    let data = { pricePerMeterId: pricePerMeterId };
    $.post({
        url: url,
        data: data,
        async: false,
        success: (response) => {
            console.log(response);
            pricePerMeter = response.pricePerMeter;
        },
    });

    return pricePerMeter;
}

function fillInitialDate(){
    let url = "api/pricePerMeter/getInitialDate";
    $.post({
        url: url,
        async: false,
        statusCode: {
            200: (response) => {
                let initialDateParts = response.split("-");
                let day = initialDateParts[2];
                let month = getStringMonth(initialDateParts[1]);
                let year = initialDateParts[0];
                $("#initialDate").val(
                    `${day} ${month}, ${year}`
                );
            },
            500: (response) => {
                console.log(response);
            }
        },
    });
}

function getStringMonth(number){
    let months = ['-','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
    return months[parseInt(number)];
}

$("#createBtn").on("click", (event) => {
    event.preventDefault();
    if (error()) return;
    create();
});

$('#updateBtn').on("click", (event) => {
    event.preventDefault();
    if(updateError()) return;
    update();
});

function updateError(){
    let priceError = validateModalPrice();

    if(priceError)
        return true;
    return false;
}

function error() {
    let priceError = validatePrice();
    let finalDateError = validateFinalDate();
    let finalDateError2 = validateFinalDate2();

    if (
        finalDateError ||
        finalDateError2 ||
        priceError
    )
        return true;
    return false;
}

function update() {
    console.log("ASDF");
    let pricePerMeterId = actualPricePerMeterId;
    let price = $('#modalPrice').val().trim();

    let url = "api/pricePerMeter/update";
    let data = {
        pricePerMeterId: pricePerMeterId,
        price: price
    };

    $.post({
        url: url,
        data: data,
        statusCode: {
            200: (response) => {
                console.log(response);
                Swal.fire({
                    type: "success",
                    title: "EXITO",
                    text: `Precio editado Exitosamente`,
                    confirmButtonText: `Aceptar`,
                }).then((response) => {
                    fillPrices();
                    //window.location = "listCliente";
                });
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

function validateFinalDate() {
    let finalDate = $("#finalDate").val().trim();
    if (finalDate == "") {
        $("#finalDateErrorMessage").removeClass("d-none");
        $("#finalDateErrorMessage").addClass("d-block");
        return true;
    } else {
        $("#finalDateErrorMessage").removeClass("d-block");
        $("#finalDateErrorMessage").addClass("d-none");
        return false;
    }
}

function validateFinalDate2() {
    let invalid;
    let finalDate = getNumericDate($("#finalDate").val());
    let url = "api/pricePerMeter/validateFinalDate";
    data = {
        finalDate: finalDate,
    };
    $.post({
        url: url,
        data: data,
        async: false,
        statusCode: {
            201: (response) => {
                invalid = true;
                $("#finalDateErrorMessage2").removeClass("d-none");
                $("#finalDateErrorMessage2").addClass("d-block");
            },
            200: (response) => {
                invalid = false;
                $("#finalDateErrorMessage2").removeClass("d-block");
                $("#finalDateErrorMessage2").addClass("d-none");
            },
        },
    });
    return invalid;
}

function validatePrice() {
    let price = $("#price").val().trim();
    if (price == "") {
        $("#priceErrorMessage").removeClass("d-none");
        $("#priceErrorMessage").addClass("d-block");
        return true;
    } else {
        $("#priceErrorMessage").removeClass("d-block");
        $("#priceErrorMessage").addClass("d-none");
        return false;
    }
}

function validateModalPrice() {
    let price = $("#modalPrice").val().trim();
    if (price == "") {
        $("#modalPriceErrorMessage").removeClass("d-none");
        $("#modalPriceErrorMessage").addClass("d-block");
        $('#modalPriceErrorMessage').focus();
        return true;
    } else {
        $("#modalPriceErrorMessage").removeClass("d-block");
        $("#modalPriceErrorMessage").addClass("d-none");
        return false;
    }
}

function create() {
    let finalDate = getNumericDate($("#finalDate").val());
    let price = $("#price").val().trim();
    let data = {
        price: price,
        finalDate: finalDate
    };
    let url = "api/pricePerMeter/create";
    $.post({
        url: url,
        data: data,
        statusCode: {
            200: (response) => {
                console.log(response);
                Swal.fire({
                    type: "success",
                    title: "EXITO",
                    text: `Usuario editado Exitosamente`,
                    confirmButtonText: `Aceptar`,
                }).then((response) => {
                    fillInitialDate();
                    fillPrices();
                    clean();
                });
            },
            500: (response) => {
                console.log(response);
            }
        },
    });
}

function clean(){
    $('#finalDate').val("");
    $('#price').val("");
}

function getNumericDate(date) {
    let dateParts = date.split(" ");
    let day = dateParts[0];
    let month = getNumericMonth(dateParts[1].substr(0, dateParts[1].length - 1));
    let year = dateParts[2];

    return `${year}/${month}/${day}`;
}

function getNumericMonth(month){
    let months = ['-','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
    return months.indexOf(month);
}
