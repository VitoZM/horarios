var actualClientId;
$(document).ready(function () {
    isLogged("read-clients");
    fillClients();
});

function read() {
    let url = "api/client/read";
    $.post({
        url: url,
        async: false,
        success: (response) => {},
    });
}

function deleteClient(clientId) {
    let url = "api/client/delete";
    let data = {
        clientId: clientId,
    };
    $.post({
        url: url,
        data: data,
        success: (response) => {
            fillClients();
            //window.location = "listCliente";
        },
    });
}

$("#deleteBtn").on("click", (event) => {
    event.preventDefault();
    deleteClient(actualClientId);
});

function cleanErrors() {
    $("#clientNumberErrorMessage").removeClass("d-block");
    $("#clientNumberErrorMessage").addClass("d-none");
    $("#clientNumberErrorMessage2").removeClass("d-block");
    $("#clientNumberErrorMessage2").addClass("d-none");
    $("#ciErrorMessage").removeClass("d-block");
    $("#ciErrorMessage").addClass("d-none");
    $("#ciErrorMessage2").removeClass("d-block");
    $("#ciErrorMessage2").addClass("d-none");
    $("#cellphoneErrorMessage").removeClass("d-block");
    $("#cellphoneErrorMessage").addClass("d-none");
    $("#addressNumberErrorMessage").removeClass("d-block");
    $("#addressNumberErrorMessage").addClass("d-none");
    $("#addressErrorMessage").removeClass("d-block");
    $("#addressErrorMessage").addClass("d-none");
    $("#birthDayErrorMessage").removeClass("d-block");
    $("#birthDayErrorMessage").addClass("d-none");
    $("#sexErrorMessage").removeClass("d-block");
    $("#sexErrorMessage").addClass("d-none");
    $("#nameErrorMessage").removeClass("d-block");
    $("#nameErrorMessage").addClass("d-none");
    $("#lastNameErrorMessage").removeClass("d-block");
    $("#lastNameErrorMessage").addClass("d-none");
}

function fillEditModal(clientId, clientNumber) {
    cleanErrors();
    actualClientId = clientId;
    let client = getClient(clientNumber);
    $("#clientNumber").val(clientNumber);
    $("#ci").val(client.ci);
    $("#name").val(client.name);
    $("#lastName").val(client.lastName);
    $("#address").val(client.address);
    $("#addressNumber").val(client.addressNumber);
    fillSexSelect(client.sex);
    let birthDayParts = client.birthDay.split("-");
    let day = birthDayParts[2];
    let month = getStringMonth(birthDayParts[1]);
    let year = birthDayParts[0];
    $("#birthDay").val(`${day} ${month}, ${year}`);
    $("#cellphone").val(client.cellphone);
    $("#payment").val(client.payment);
}

function getStringMonth(number) {
    let months = [
        "-",
        "Enero",
        "Febrero",
        "Marzo",
        "Abril",
        "Mayo",
        "Junio",
        "Julio",
        "Agosto",
        "Septiembre",
        "Octubre",
        "Noviembre",
        "Diciembre",
    ];
    return months[parseInt(number)];
}

function fillSexSelect(sex) {
    $("#sex").empty();
    let selected = false;
    if (sex == "Masculino") selected = true;
    let $option = $("<option />", {
        text: "Masculino",
        value: "Masculino",
        selected: selected,
        "data-icon": "feather icon-user",
    });
    $("#sex").prepend($option);
    selected = false;
    if (sex == "Femenino") selected = true;
    $option = $("<option />", {
        text: "Femenino",
        value: "Femenino",
        selected: selected,
        "data-icon": "feather icon-user",
    });
    $("#sex").prepend($option);
}

$("#updateBtn").on("click", (event) => {
    event.preventDefault();
    if (error()) return;
    update();
    $("#editarCliente").modal("hide");
});

function error() {
    let cellphoneError = validateCellphone();
    let birthDayError = validateBirthDay();
    let sexError = validateSex();
    let addressNumberError = validateAddressNumber();
    let addressError = validateAddress();
    let lastNameError = validateLastName();
    let nameError = validateName();
    let ciError = validateCi();
    let ciError2 = validateCi2();
    let clientNumberError2 = validateClientNumber2();
    let clientNumberError = validateClientNumber();

    if (
        lastNameError ||
        nameError ||
        addressError ||
        addressNumberError ||
        birthDayError ||
        sexError ||
        cellphoneError ||
        clientNumberError ||
        clientNumberError2 ||
        ciError ||
        ciError2
    )
        return true;
    return false;
}

function validateCi() {
    let ci = $("#ci").val().trim();
    if (ci == "") {
        $("#ciErrorMessage").removeClass("d-none");
        $("#ciErrorMessage").addClass("d-block");
        $("#ci").focus();
        return true;
    } else {
        $("#ciErrorMessage").removeClass("d-block");
        $("#ciErrorMessage").addClass("d-none");
        return false;
    }
}

function validateCi2() {
    let invalid;
    let ci = $("#ci").val().trim();
    let clientId = actualClientId;
    let url = "api/client/findByCiByClientId";
    $.post({
        url: url,
        data: {
            ci: ci,
            clientId: clientId,
        },
        async: false,
        statusCode: {
            200: (response) => {
                invalid = true;
                $("#ciErrorMessage2").removeClass("d-none");
                $("#ciErrorMessage2").addClass("d-block");
                $("#ci").focus();
            },
            500: (response) => {
                invalid = false;
                $("#ciErrorMessage2").removeClass("d-block");
                $("#ciErrorMessage2").addClass("d-none");
            },
        },
    });
    return invalid;
}

function validateClientNumber() {
    let clientNumber = $("#clientNumber").val().trim();
    if (clientNumber == "") {
        $("#clientNumberErrorMessage").removeClass("d-none");
        $("#clientNumberErrorMessage").addClass("d-block");
        $("#clientNumber").focus();
        return true;
    } else {
        $("#clientNumberErrorMessage").removeClass("d-block");
        $("#clientNumberErrorMessage").addClass("d-none");
        return false;
    }
}

function validateClientNumber2() {
    let invalid;
    let clientId = actualClientId;
    let clientNumber = $("#clientNumber").val().trim();
    let url = "api/client/findByClientNumberByClientId";
    $.post({
        url: url,
        data: {
            clientNumber: clientNumber,
            clientId: clientId,
        },
        async: false,
        statusCode: {
            200: (response) => {
                invalid = true;
                $("#clientNumberErrorMessage2").removeClass("d-none");
                $("#clientNumberErrorMessage2").addClass("d-block");
                $("#clientNumber").focus();
            },
            500: (response) => {
                invalid = false;
                $("#clientNumberErrorMessage2").removeClass("d-block");
                $("#clientNumberErrorMessage2").addClass("d-none");
            },
        },
    });
    return invalid;
}

function validateCellphone() {
    let cellphone = $("#cellphone").val().trim();
    if (cellphone == "") {
        $("#cellphoneErrorMessage").removeClass("d-none");
        $("#cellphoneErrorMessage").addClass("d-block");
        $("#cellphone").focus();
        return true;
    } else {
        $("#cellphoneErrorMessage").removeClass("d-block");
        $("#cellphoneErrorMessage").addClass("d-none");
        return false;
    }
}

function validateBirthDay() {
    let birthDay = $("#birthDay").val().trim();
    if (birthDay == "") {
        $("#birthDayErrorMessage").removeClass("d-none");
        $("#birthDayErrorMessage").addClass("d-block");
        return true;
    } else {
        $("#birthDayErrorMessage").removeClass("d-block");
        $("#birthDayErrorMessage").addClass("d-none");
        return false;
    }
}

function validateSex() {
    let sex = $("#sex").val();
    if (sex == null || (sex == "0") | (sex == "")) {
        $("#sexErrorMessage").removeClass("d-none");
        $("#sexErrorMessage").addClass("d-block");
        $("#sex").focus();
        return true;
    } else {
        $("#sexErrorMessage").removeClass("d-block");
        $("#sexErrorMessage").addClass("d-none");
        return false;
    }
}

function validateAddress() {
    let address = $("#address").val().trim();
    if (address == "") {
        $("#addressErrorMessage").removeClass("d-none");
        $("#addressErrorMessage").addClass("d-block");
        $("#address").focus();
        return true;
    } else {
        $("#addressErrorMessage").removeClass("d-block");
        $("#addressErrorMessage").addClass("d-none");
        return false;
    }
}

function validateAddressNumber() {
    let addressNumber = $("#addressNumber").val().trim();
    if (addressNumber == "") {
        $("#addressNumberErrorMessage").removeClass("d-none");
        $("#addressNumberErrorMessage").addClass("d-block");
        $("#addressNumber").focus();
        return true;
    } else {
        $("#addressNumberErrorMessage").removeClass("d-block");
        $("#addressNumberErrorMessage").addClass("d-none");
        return false;
    }
}

function validateName() {
    let name = $("#name").val().trim();
    if (name == "") {
        $("#nameErrorMessage").removeClass("d-none");
        $("#nameErrorMessage").addClass("d-block");
        $("#name").focus();
        return true;
    } else {
        $("#nameErrorMessage").removeClass("d-block");
        $("#nameErrorMessage").addClass("d-none");
        return false;
    }
}

function validateLastName() {
    let name = $("#lastName").val().trim();
    if (name == "") {
        $("#lastNameErrorMessage").removeClass("d-none");
        $("#lastNameErrorMessage").addClass("d-block");
        $("#lastName").focus();
        return true;
    } else {
        $("#lastNameErrorMessage").removeClass("d-block");
        $("#lastNameErrorMessage").addClass("d-none");
        return false;
    }
}

function update() {
    let clientId = actualClientId;
    let ci = $('#ci').val().trim();
    let clientNumber = $('#clientNumber').val().trim();
    let name = format($("#name").val());
    let lastName = format($("#lastName").val());
    let address = format($("#address").val());
    let addressNumber = $("#addressNumber").val().trim();
    let sex = $("#sex").val();
    let birthDay = getNumericDate($("#birthDay").val());
    let cellphone = $("#cellphone").val().trim();

    let url = "api/client/update";
    $.post({
        url: url,
        data: {
            clientId: clientId,
            ci: ci,
            clientNumber: clientNumber,
            name: name,
            lastName: lastName,
            address: address,
            addressNumber: addressNumber,
            sex: sex,
            birthDay: birthDay,
            cellphone: cellphone,
        },
        statusCode: {
            200: (response) => {
                console.log(response);
                Swal.fire({
                    type: "success",
                    title: "EXITO",
                    text: `Cliente editado Exitosamente`,
                    confirmButtonText: `Aceptar`,
                })
                fillClients();
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

function getClient(clientNumber) {
    let client;
    let url = "api/client/findByClientNumber";
    let data = { clientNumber: clientNumber };
    $.post({
        url: url,
        data: data,
        async: false,
        success: (response) => {
            client = response.client;
        },
    });
    return client;
}

function getNumericDate(date) {
    let dateParts = date.split(" ");
    let day = dateParts[0];
    let month = getNumericMonth(
        dateParts[1].substr(0, dateParts[1].length - 1)
    );
    let year = dateParts[2];

    return `${year}/${month}/${day}`;
}

function getNumericMonth(month) {
    let months = [
        "-",
        "Enero",
        "Febrero",
        "Marzo",
        "Abril",
        "Mayo",
        "Junio",
        "Julio",
        "Agosto",
        "Septiembre",
        "Octubre",
        "Noviembre",
        "Diciembre",
    ];
    return months.indexOf(month);
}

function format(string) {
    let result = string.trim().toLowerCase();
    result = result.replace(/\b\w/g, (l) => l.toUpperCase());
    return result;
}
