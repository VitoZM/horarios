function getToken() {
    let url = "token";
    let fullToken;
    $.ajax({
        url: url,
        type: "GET",
        async: false,
        success: (result) => {
            fullToken = result;
        },
    });
    let token = getShortToken(fullToken);
    return token;
}

function getShortToken(fullToken) {
    let token = "";
    let finish = 0;
    for (let i = fullToken.length - 1; ; i--) {
        if (fullToken[i] == '"') {
            finish = finish + 1;
            if (finish == 2) break;
            continue;
        }
        if (finish == 1) token += fullToken[i];
    }

    return token.split("").reverse().join("");
}
