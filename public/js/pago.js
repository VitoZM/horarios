isLogged("update-billings");
fillSelectMeasurerId();
fillSelectClientId();

$("#searchBtn").on("click", function (event) {
    event.preventDefault();
    if (error()) return;
    fillTable();
});

$("#showPrice").on("click", () => {
    let maxId = getMaxIdChecked();
    let totalPrice = getTotalPrice(maxId);
    let totalBillings = getTotalBillings(maxId);
    $('#totalPrice').html(totalPrice + " Bs.");
    $('#totalBillings').html(totalBillings);
});

function getCheckedBillings(maxId){
    let checkedBillings = [];
    $(".checkbox").each(function () {
        let id = $(this).val().split(",")[0];
        if (id <= maxId)
            checkedBillings.push(id);
    });
    return checkedBillings;
}

$("#updateBtn").on("click", event => {
    event.preventDefault();
    let maxId = getMaxIdChecked();
    let checkedBillings = getCheckedBillings(maxId);
    let url = "api/billing/update";
    let data = {
        checkedBillings: checkedBillings
    };
    $.post({
        url: url,
        data: data,
        async: false,
        statusCode: {
            200: (response) => {
                fillTable();
                Swal.fire({
                    type: "success",
                    title: "EXITO",
                    text: `Las facturas se guardaran en formato pdf`,
                    confirmButtonText: `Aceptar`,
                }).then((response) => {
                    window.location = '#head';
                });
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
});

function getTotalPrice(maxId){
    let totalPrice = 0;
    $(".checkbox").each(function () {
        let id = $(this).val().split(",")[0];
        if (id <= maxId){
            let price = $(this).val().split(",")[1];
            totalPrice += parseFloat(price);
        }
    });
    return totalPrice;
}

function getMaxIdChecked() {
    let maxId = 0;
    $(".checkbox").each(function () {
        let id = $(this).val().split(",")[0];
        if ($(this).prop("checked") && id >= maxId)
            maxId = id;
    });
    return maxId;
}

function getTotalBillings(maxId){
    let totalBillings = 0;
    $(".checkbox").each(function () {
        let id = $(this).val().split(",")[0];
        if (id <= maxId)
            totalBillings++;
    });
    return totalBillings;
}

function error() {
    let measurerIdError = validateMeasurerId();
    let clientIdError = validateClientId();
    if (measurerIdError || clientIdError) return true;
    return false;
}

function validateMeasurerId() {
    let measurerId = $("#measurerId").val();
    if (measurerId == null || measurerId == "") {
        $("#measurerIdErrorMessage").removeClass("d-none");
        $("#measurerIdErrorMessage").addClass("d-block");
        return true;
    } else {
        $("#measurerIdErrorMessage").removeClass("d-block");
        $("#measurerIdErrorMessage").addClass("d-none");
        return false;
    }
}

function validateClientId() {
    let clientId = $("#clientId").val();
    if (clientId == "" || clientId == null) {
        $("#clientIdErrorMessage").removeClass("d-none");
        $("#clientIdErrorMessage").addClass("d-block");
        return true;
    } else {
        $("#clientIdErrorMessage").removeClass("d-block");
        $("#clientIdErrorMessage").addClass("d-none");
        return false;
    }
}

function fillTable() {
    let billings = getBillings();
    if (billings.length != "0") {
        fillBillings();
        $("#showPrice").removeAttr("disabled");
    } else{
        document.getElementById("myGrid").innerHTML = "<h1>No Tiene Facturas Por Pagar</h1>";
        $("#showPrice").attr("disabled","true");
    }
}

$(document).on("change", ".checkbox", function () {
    let limit = $(this).val().split("-")[0];
    $(".checkbox").each(function () {
        if ($(this).val() <= limit) $(this).prop("checked", true);
        else $(this).prop("checked", false);
    });
});

function fillHeader(client) {
    if(client != null){
        $("#clientNumber").html(client.clientNumber);
        $("#measurerNumber").html(client.measurerNumber);
        $("#clientName").html(client.lastName + " " + client.name);
    }
    else{
        Swal.fire({
            type: "error",
            title: "ERROR",
            text: `El número de cliente no esta asociado al número de medidor`,
            confirmButtonText: `Aceptar`,
        }).then((response) => {
            window.location = '#head';
        });
        $("#clientNumber").html("-");
        $("#measurerNumber").html("-");
        $("#clientName").html("-");
    }
}

function getBillings() {
    let billings;
    let clientId = $("#clientId").val();
    let measurerId = $("#measurerId").val();
    let url = "api/billing/getBillings";
    let data = {
        clientId: clientId,
        measurerId: measurerId,
    };
    $.post({
        url: url,
        data: data,
        async: false,
        statusCode: {
            200: (response) => {
                console.log(response);
                cleanErrors();
                window.location = "#basic-examples";
                fillHeader(response.client);
                billings = response.billings;
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
    return billings;
}

function cleanErrors() {
    $("#clientIdErrorMessage").removeClass("d-block");
    $("#clientIdErrorMessage").addClass("d-none");
    $("#measurerIdErrorMessage").removeClass("d-block");
    $("#measurerIdErrorMessage").addClass("d-none");
}

function fillSelectMeasurerId() {
    $("#measurerId").empty();
    $option = $("<option />", {
        text: "N° Medidor",
        value: "",
        readonly: true,
    });
    $("#measurerId").prepend($option);
    let url = "api/measurer/read";
    $.post({
        url: url,
        async: false,
        statusCode: {
            200: (response) => {
                let measurers = response.measurers;
                measurers.forEach((measurer) => {
                    $option = $("<option />", {
                        text: measurer.measurerNumber,
                        value: measurer.measurerId,
                    });
                    $("#measurerId").prepend($option);
                });
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

function fillSelectClientId() {
    let url = "api/client/read";
    $.post({
        url: url,
        async: false,
        statusCode: {
            200: (response) => {
                clients = response.clients;
                clients.forEach((client) => {
                    $option = $("<option />", {
                        text: client.clientNumber,
                        value: client.clientId,
                    });
                    $("#clientId").prepend($option);
                });
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}
