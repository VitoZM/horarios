var actualReadingId;

$(document).ready(function () {
    isLogged("read-readings");
    fillReadings();
});

function setImage(image){
    let src = 'app-assets/images/readings/'+image;
    $('#modalImage').attr('src',src)
}

function read() {
    let url = "api/reading/read";
    $.post({
        url: url,
        async: false,
        success: (response) => {

        },
    });
}

function fillEditModal(readingId){
    actualReadingId = readingId;
    cleanErrors();
}
