(function (window, undefined) {
  'use strict';

  // muestra del file-upload
  $('.tipoLectura').on('change', function () {
    if (this.value === '1') {
      $('.file-upload').addClass('d-block');
    } else {
      $('.file-upload').removeClass('d-block');
    }
    if (this.value === '2') {
      $('.registroConsumo').prop('disabled', true);
      $('.registroConsumo').val('30');
    } else {
      $('.registroConsumo').prop('disabled', false);
      $('.registroConsumo').val('');
    }
  });

  $(document).on("wheel", "input[type=number]", function (e) {
    $(this).blur();
  });  //deshacerse del scroll en number

  // $(function () {

  //   // elementos de la lista
  //   var menues = $(".main-menu-content li a");

  //   // manejador de click sobre todos los elementos
  //   menues.click(function () {
  //     // eliminamos active de todos los elementos
  //     menues.removeClass("active");
  //     // activamos el elemento clicado.
  //     $(this).addClass("active");
  //   });

  // });
  // $(document).on('click', '.main-menu-content li a', function () {
  //   $(this).addClass('active').siblings().removeClass('active');
  // });

  // $('.main-menu-content li a').filter(function () {
  //   return this.href == location.href.replace(/#.*/, "");
  // }).parents('li').addClass('active');

  // $(function () {
  //   var current = location.pathname;
  //   $('.main-menu-content li a').each(function () {
  //     var $this = $(this);
  //     // if the current path is like this link, make it active
  //     if ($this.attr('href').indexOf(current) !== -1) {
  //       $this.addClass('active');
  //     }
  //   })
  // })


})(window);