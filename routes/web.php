<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('page');
});

/*Route::post('/admin', function () {
    return view('admin');
})->middleware('jwt.verify:user');*/

Route::get('/{page}', function ($page) {
    try{
        return view($page);
    }catch(Throwable $t){
        abort(404,'Page not found');
        abort(403);
    }
});
