<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LogController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\MeasurerController;
use App\Http\Controllers\ReadingController;
use App\Http\Controllers\BillingController;
use App\Http\Controllers\ConfigController;
use App\Http\Controllers\RegistrationPriceController;
use App\Http\Controllers\PricePerMeterController;
use App\Http\Controllers\DiscountController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\IncomeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/admin', function () {
    return view('admin');
})->middleware('permission:create-clients');

Route::post('/user/create', [UserController::class, 'create']);
Route::post('/user/read', [UserController::class, 'read']);
Route::post('/user/update', [UserController::class, 'update']);
Route::post('/user/delete', [UserController::class, 'delete']);
Route::post('/user/findByUserName', [UserController::class, 'findByUserName']);
Route::post('/user/quantity', [UserController::class, 'quantity']);

Route::post('/client/create', [ClientController::class, 'create']);
Route::post('/client/read', [ClientController::class, 'read']);
Route::post('/client/update', [ClientController::class, 'update']);
Route::post('/client/delete', [ClientController::class, 'delete']);
Route::post('/client/findByClientNumber', [ClientController::class, 'findByClientNumber']);
Route::post('/client/findByCi', [ClientController::class, 'findByCi']);
Route::post('/client/findByClientNumberByClientId', [ClientController::class, 'findByClientNumberByClientId']);
Route::post('/client/findByCiByClientId', [ClientController::class, 'findByCiByClientId']);
Route::post('/client/quantity', [ClientController::class, 'quantity']);

Route::post('/measurer/create', [MeasurerController::class, 'create']);
Route::post('/measurer/read', [MeasurerController::class, 'read']);
Route::post('/measurer/update', [MeasurerController::class, 'update']);
Route::post('/measurer/changeStatus', [MeasurerController::class, 'changeStatus']);
Route::post('/measurer/findByMeasurerNumber', [MeasurerController::class, 'findByMeasurerNumber']);
Route::post('/measurer/findById', [MeasurerController::class, 'findById']);
Route::post('/measurer/findByMeasurerNumberByMeasurerId', [MeasurerController::class, 'findByMeasurerNumberByMeasurerId']);
Route::post('/measurer/quantity', [MeasurerController::class, 'quantity']);
Route::post('/measurer/readOnServiceToOutage', [MeasurerController::class, 'readOnServiceToOutage']);
Route::post('/measurer/outage', [MeasurerController::class, 'outage']);
Route::post('/measurer/readOnOutageToService', [MeasurerController::class, 'readOnOutageToService']);
Route::post('/measurer/service', [MeasurerController::class, 'service']);

Route::post('/reading/create', [ReadingController::class, 'create']);
Route::post('/reading/read', [ReadingController::class, 'read']);
Route::post('/reading/update', [ReadingController::class, 'update']);
Route::post('/reading/delete', [ReadingController::class, 'delete']);
Route::post('/reading/isValid', [ReadingController::class, 'isValid']);
Route::post('/reading/readPendingReadings', [ReadingController::class, 'readPendingReadings']);

Route::post('/billing/getBillings', [BillingController::class, 'getBillings']);
Route::post('/billing/getAllBillings', [BillingController::class, 'getAllBillings']);
Route::post('/billing/update', [BillingController::class, 'update']);

Route::post('/log/create', [LogController::class, 'create']);
Route::post('/log/read', [LogController::class, 'read']);
Route::post('/log/update', [LogController::class, 'update']);

Route::post('/profile/read', [ProfileController::class, 'read']);
Route::post('/profile/update', [ProfileController::class, 'update']);
Route::post('/profile/getProfile', [ProfileController::class, 'getProfile']);

Route::post('/config/upload', [ConfigController::class, 'upload']);
Route::post('/config/isUploaded', [ConfigController::class, 'isUploaded']);

Route::post('/pricePerMeter/create', [PricePerMeterController::class, 'create']);
Route::post('/pricePerMeter/read', [PricePerMeterController::class, 'read']);
Route::post('/pricePerMeter/update', [PricePerMeterController::class, 'update']);
Route::post('/pricePerMeter/validateFinalDate', [PricePerMeterController::class, 'validateFinalDate']);
Route::post('/pricePerMeter/getInitialDate', [PricePerMeterController::class, 'getInitialDate']);
Route::post('/pricePerMeter/findById', [PricePerMeterController::class, 'findById']);

Route::post('/discount/create', [DiscountController::class, 'create']);
Route::post('/discount/read', [DiscountController::class, 'read']);
Route::post('/discount/update', [DiscountController::class, 'update']);
Route::post('/discount/validateMaximumAge', [DiscountController::class, 'validateMaximumAge']);
Route::post('/discount/getMinimumAge', [DiscountController::class, 'getMinimumAge']);
Route::post('/discount/findById', [DiscountController::class, 'findById']);

Route::post('/registrationPrice/read', [RegistrationPriceController::class, 'read']);
Route::post('/registrationPrice/update', [RegistrationPriceController::class, 'update']);

Route::post('/income/read', [IncomeController::class, 'read']);

Route::post('/permission/read', [PermissionController::class, 'read']);

// @TODO: Auth
Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::post('me', [AuthController::class, 'me']);

});
