<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MeasurerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('measurers')->insert([
            'measurerId' => 1,
            'measurerNumber' => 1516168,
            'description' => 'PRIMER MEDIDOR AGREGADO',
            'installedAddress' => 'Loa',
            'zone' => 'Mercado Central',
            'registerDate' => '2020/10/01',
            'clientId' => 1,
            'created_at' => '2020/11/21 08:00:00',
            'updated_at' => '2020/11/21 08:00:00'
        ]);

    }
}
