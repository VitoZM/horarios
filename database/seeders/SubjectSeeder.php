<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subjects')->insert([
            'subjectId' => 1,
            'name' => 'PROGRAMACIÓN BÁSICA',
            'initials' => 'SIS100',
            'HT' => 2,
            'HL' => 4,
            'TH' => 6,
            'CR' => 4,
            'description' => 'Aqui aprendemos a programar',
            'semesterId' => 1,
            'created_at' => '2021/01/14 08:00:00',
            'updated_at' => '2021/01/14 08:00:00'
        ]);
    }
}
