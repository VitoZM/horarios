<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        //dd(UserSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(ProfileSeeder::class);
        $this->call(LogSeeder::class);
        $this->call(ClientSeeder::class);
        $this->call(MeasurerSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(RoleHasPermissionSeeder::class);
        $this->call(ModelHasRoleSeeder::class);
        $this->call(ReadingSeeder::class);
        $this->call(BillingSeeder::class);
        $this->call(PricePerMeterSeeder::class);
        $this->call(DiscountSeeder::class);
        $this->call(RegistrationPriceSeeder::class);
        $this->call(IncomeSeeder::class);
        $this->call(PenaltyTypeSeeder::class);
        $this->call(PenaltyHistorySeeder::class);
    }
}
