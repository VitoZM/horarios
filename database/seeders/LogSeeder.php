<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('logs')->insert([
            'logId' => 1,
            'entry' => '2020/11/23 18:15:33',
            'departure' => '2020/11/23 21:05:03',
            'duration' => "0 meses 0 days 3 horas 1 minutos 3 segundos",
            'status' => '0',
            'userId' => 1
        ]);

        DB::table('logs')->insert([
            'logId' => 2,
            'entry' => '2020/11/23 18:15:33',
            'departure' => '2020/11/23 21:05:03',
            'duration' => "0 meses 0 days 3 horas 25 minutos 45 segundos",
            'status' => '0',
            'userId' => 2
        ]);
    }
}
