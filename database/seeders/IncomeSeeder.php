<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class IncomeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('incomes')->insert([
            'incomeId' => 1,
            'amount' => 151.05,
            'modelType' => 'App\Models\Billing',
            'modelId' => 1,
            'created_at' => '2020/01/01',
            'updated_at' => '2020/01/01'
        ]);

        DB::table('incomes')->insert([
            'incomeId' => 2,
            'amount' => 285,
            'modelType' => 'App\Models\Billing',
            'modelId' => 2,
            'created_at' => '2020/01/01',
            'updated_at' => '2020/01/01'
        ]);

        DB::table('incomes')->insert([
            'incomeId' => 3,
            'amount' => 100.5,
            'modelType' => 'App\Models\Client',
            'modelId' => 1,
            'created_at' => '2020/01/01',
            'updated_at' => '2020/01/01'
        ]);
    }
}
