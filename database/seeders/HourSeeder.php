<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HourSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('hours')->insert([
            'hourId' => 1,
            'initialHour' => '07:00',
            'finalHour' => '08:00',
            'created_at' => '2021/01/14 08:00:00',
            'updated_at' => '2021/01/14 08:00:00'
        ]);

        DB::table('hours')->insert([
            'hourId' => 2,
            'initialHour' => '08:00',
            'finalHour' => '09:00',
            'created_at' => '2021/01/14 08:00:00',
            'updated_at' => '2021/01/14 08:00:00'
        ]);
    }
}
