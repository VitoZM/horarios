<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PricePerMeterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('price_per_meters')->insert([
            'pricePerMeterId' => 1,
            'price' => 1.5,
            'initialDate' => '2020/01/01',
            'finalDate' => "2020/12/31",
            'created_at' => '2020/01/01',
            'updated_at' => '2020/01/01'
        ]);
    }
}
