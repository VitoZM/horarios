<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('files')->insert([
            'teacherId' => 1,
            'subjectId' => 1,
            'created_at' => '2021/01/14 08:00:00',
            'updated_at' => '2021/01/14 08:00:00'
        ]);
    }
}
