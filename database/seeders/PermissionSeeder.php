<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create(['name' => 'users-module', 'guard_name' => 'web']);
        Permission::create(['name' => 'create-users', 'guard_name' => 'web']);
        Permission::create(['name' => 'read-users', 'guard_name' => 'web']);
        Permission::create(['name' => 'update-users', 'guard_name' => 'web']);
        Permission::create(['name' => 'delete-users', 'guard_name' => 'web']);

        Permission::create(['name' => 'clients-module', 'guard_name' => 'web']);
        Permission::create(['name' => 'create-clients', 'guard_name' => 'web']);
        Permission::create(['name' => 'read-clients', 'guard_name' => 'web']);
        Permission::create(['name' => 'update-clients', 'guard_name' => 'web']);
        Permission::create(['name' => 'delete-clients', 'guard_name' => 'web']);

        Permission::create(['name' => 'measurers-module', 'guard_name' => 'web']);
        Permission::create(['name' => 'create-measurers', 'guard_name' => 'web']);
        Permission::create(['name' => 'read-measurers', 'guard_name' => 'web']);
        Permission::create(['name' => 'update-measurers', 'guard_name' => 'web']);
        Permission::create(['name' => 'delete-measurers', 'guard_name' => 'web']);

        Permission::create(['name' => 'readings-module', 'guard_name' => 'web']);
        Permission::create(['name' => 'create-readings', 'guard_name' => 'web']);
        Permission::create(['name' => 'read-readings', 'guard_name' => 'web']);
        Permission::create(['name' => 'update-readings', 'guard_name' => 'web']);
        Permission::create(['name' => 'delete-readings', 'guard_name' => 'web']);

        Permission::create(['name' => 'billings-module', 'guard_name' => 'web']);
        Permission::create(['name' => 'read-billings', 'guard_name' => 'web']);
        Permission::create(['name' => 'update-billings', 'guard_name' => 'web']);

        Permission::create(['name' => 'price-module', 'guard_name' => 'web']);
        Permission::create(['name' => 'create-price', 'guard_name' => 'web']);
        Permission::create(['name' => 'read-price', 'guard_name' => 'web']);
        Permission::create(['name' => 'update-price', 'guard_name' => 'web']);

        Permission::create(['name' => 'discount-module', 'guard_name' => 'web']);
        Permission::create(['name' => 'create-discount', 'guard_name' => 'web']);
        Permission::create(['name' => 'read-discount', 'guard_name' => 'web']);
        Permission::create(['name' => 'update-discount', 'guard_name' => 'web']);

        Permission::create(['name' => 'read-incomes', 'guard_name' => 'web']);
        Permission::create(['name' => 'read-logs', 'guard_name' => 'web']);
    }
}
