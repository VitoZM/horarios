<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('schedules')->insert([
            'scheduleId' => 1,
            'management' => '2/2020',
            'groupId' => 1,
            'subjectId' => 1,
            'hourId' => 1,
            'classroomId' => 1,
            'created_at' => '2021/01/14 08:00:00',
            'updated_at' => '2021/01/14 08:00:00'
        ]);

        DB::table('schedules')->insert([
            'scheduleId' => 2,
            'management' => '2/2020',
            'groupId' => 1,
            'subjectId' => 1,
            'hourId' => 2,
            'classroomId' => 1,
            'created_at' => '2021/01/14 08:00:00',
            'updated_at' => '2021/01/14 08:00:00'
        ]);
    }
}
