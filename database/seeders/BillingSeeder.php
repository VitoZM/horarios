<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BillingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('billings')->insert([
            'readingId' => 1,
            'pricePerMeter' => 1.5,
            'consumption' => 106,
            'billingDate' => '2020/10/01',
            'billingPrice' => 159,
            'discountPercentage' => 5,
            'totalBillingPrice' => 151.05,
            'status' => '1',
            'paymentDate' => '2020/11/15 15:00:00',
            'expirationDate' => '2021/01/01'
        ]);

        DB::table('billings')->insert([
            'readingId' => 2,
            'pricePerMeter' => 1.5,
            'consumption' => 200,
            'billingDate' => '2020/11/01',
            'billingPrice' => 300,
            'discountPercentage' => 5,
            'totalBillingPrice' => 285,
            'expirationDate' => '2021/02/01'
        ]);
    }
}
