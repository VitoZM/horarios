<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ReadingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('readings')->insert([
            'readingId' => 1,
            'measurerReading' => 106,
            'type' => '1',
            'image' => '1.jpg',
            'description' => 'Primer lectura',
            'measurerId' => 1,
            'userId' => 1,
            'created_at' => '2020/12/01 08:00:00',
            'updated_at' => '2020/12/01 08:00:00'
        ]);

        DB::table('readings')->insert([
            'readingId' => 2,
            'measurerReading' => 306,
            'type' => '1',
            'image' => '2.jpg',
            'description' => 'Segunda lectura',
            'measurerId' => 1,
            'userId' => 2,
            'created_at' => '2021/01/01 08:00:00',
            'updated_at' => '2021/01/01 08:00:00'
        ]);
    }
}
