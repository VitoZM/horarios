<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DiscountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('discounts')->insert([
            'discountId' => 1,
            'minimumAge' => 60,
            'maximumAge' => 70,
            'discountPercentage' => 5,
            'created_at' => '2020/01/01',
            'updated_at' => '2020/01/01'
        ]);
    }
}
