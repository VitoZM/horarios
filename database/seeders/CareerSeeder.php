<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CareerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('careers')->insert([
            'careerId' => 1,
            'name' => 'Ingeniería en Ciencias de la Computación',
            'initials' => 'CICO',
            'description' => 'Ingeniería similar a la ingeniera de software',
            'created_at' => '2021/01/14 08:00:00',
            'updated_at' => '2021/01/14 08:00:00'
        ]);
    }
}
