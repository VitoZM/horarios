<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;

class RoleHasPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::find(1);

        $permissions = DB::table('permissions')->get();

        foreach($permissions as $permission)
            $role->givePermissionTo($permission->name);

        $role = Role::find(2);
        $role->givePermissionTo('users-module');//permiso de estar en página administrador

        $role->givePermissionTo('clients-module');
        $role->givePermissionTo('create-clients');
        $role->givePermissionTo('read-clients');
        $role->givePermissionTo('update-clients');
        $role->givePermissionTo('delete-clients');

        $role->givePermissionTo('billings-module');
        $role->givePermissionTo('read-billings');
        $role->givePermissionTo('update-billings');

        $role->givePermissionTo('measurers-module');
        $role->givePermissionTo('create-measurers');
        $role->givePermissionTo('read-measurers');
        $role->givePermissionTo('update-measurers');
        $role->givePermissionTo('delete-measurers');

        $role = Role::find(3);
        $role->givePermissionTo('users-module');

        $role->givePermissionTo('readings-module');
        $role->givePermissionTo('create-readings');
        $role->givePermissionTo('read-readings');

        $role->givePermissionTo('read-measurers');
        $role->givePermissionTo('update-measurers');
    }
}
