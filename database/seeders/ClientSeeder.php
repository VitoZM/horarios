<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clients')->insert([
            'clientId' => 1,
            'clientNumber' => 15168168,
            'ci' => '1385416',
            'name' => 'Juan José',
            'lastName' => 'Zurita Sabal',
            'sex' => 'Masculino',
            'birthDay' => '1990/11/20',
            'address' => 'Loa',
            'addressNumber' => '97',
            'cellphone' => '72861652',
            'payment' => 100.5,
            'created_at' => '2020/11/21 08:00:00',
            'updated_at' => '2020/11/21 08:00:00'
        ]);

    }
}
