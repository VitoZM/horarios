<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_accounts')->insert([
            'userId' => 1,
            'userName' => 'vito',
            'password' => Hash::make('1234'),
            'roleId' => 1,
            'created_at' => '2020/11/21 08:00:00',
            'updated_at' => '2020/11/21 08:00:00'
        ]);

        DB::table('user_accounts')->insert([
            'userId' => 2,
            'userName' => 'bert',
            'password' => Hash::make('1234'),
            'roleId' => 1,
            'created_at' => '2020/11/21 08:00:00',
            'updated_at' => '2020/11/21 08:00:00'
        ]);
    }
}
