<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PenaltyTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('penalty_types')->insert([
            'penaltyTypeId' => 1,
            'code' => 'P-001',
            'cost' => 25,
            'description' => 'Multa por mora',
            'created_at' => '2020/11/21 08:00:00',
            'updated_at' => '2020/11/21 08:00:00'
        ]);
    }
}
