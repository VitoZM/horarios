<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profiles')->insert([
            'userId' => 1,
            'name' => 'Alvaro',
            'lastName' => 'Zapata',
            'sex' => 'Masculino',
            'birthDay' => '2000/01/01',
            'job' => 'Administrador',
            'department' => 'Sistemas',
            'entryDate' => '2020/11/20 09:00:00',
            'created_at' => '2020/11/21 08:00:00',
            'updated_at' => '2020/11/21 08:00:00'
        ]);

        DB::table('profiles')->insert([
            'userId' => 2,
            'name' => 'Alberto',
            'lastName' => 'Mamani',
            'sex' => 'Masculino',
            'birthDay' => '2002/01/01',
            'job' => 'Administrador',
            'department' => 'Sistemas',
            'entryDate' => '2020/11/21 09:00:00',
            'created_at' => '2020/11/21 08:00:00',
            'updated_at' => '2020/11/21 08:00:00'
        ]);

        DB::table('profiles')->insert([
            'userId' => 3,
            'name' => 'Rigoberto',
            'lastName' => 'Miranda',
            'sex' => 'Masculino',
            'birthDay' => '2002/01/01',
            'job' => 'Administrador',
            'department' => 'Sistemas',
            'entryDate' => '2020/11/21 09:00:00',
            'created_at' => '2020/11/21 08:00:00',
            'updated_at' => '2020/11/21 08:00:00'
        ]);
    }
}
