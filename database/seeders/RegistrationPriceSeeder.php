<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RegistrationPriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('registration_prices')->insert([
            'registrationPriceId' => 1,
            'price' => 100,
            'created_at' => '2020/12/01 08:00:00',
            'updated_at' => '2020/12/01 08:00:00'
        ]);
    }
}
