<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->unsignedInteger('teacherId');
            $table->unsignedInteger('subjectId');
            $table->string('enabled',1)->default('1');
            $table->timestamps();
            $table->primary(['teacherId','subjectId']);
            $table->foreign('teacherId')->references('teacherId')->on('teachers');
            $table->foreign('subjectId')->references('subjectId')->on('subjects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
