<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->increments('scheduleId');
            $table->string('management',10);
            $table->unsignedInteger('groupId');
            $table->unsignedInteger('subjectId');
            $table->unsignedInteger('hourId');
            $table->unsignedInteger('classroomId');
            $table->timestamps();
            $table->foreign('groupId')->references('groupId')->on('groups');
            $table->foreign('subjectId')->references('subjectId')->on('subjects');
            $table->foreign('hourId')->references('hourId')->on('hours');
            $table->foreign('classroomId')->references('classroomId')->on('classrooms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}
