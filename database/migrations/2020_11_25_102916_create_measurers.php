<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMeasurers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('measurers', function (Blueprint $table) {
            $table->increments('measurerId');
            $table->integer('measurerNumber')->unique();
            $table->text('description')->nullable()->default("");
            $table->string('installedAddress',30);
            $table->string('zone',30);
            $table->string('status',1)->default("1");
            $table->date('registerDate');
            $table->unsignedInteger('clientId');
            $table->timestamps();
            $table->foreign('clientId')->references('clientId')->on('clients');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('measurers');
    }
}
