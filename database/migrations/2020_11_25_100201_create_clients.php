<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('clientId');
            $table->integer('clientNumber')->unique();
            $table->string('ci',30)->unique();
            $table->string('name',30);
            $table->string('lastName',30);
            $table->string('sex',10);
            $table->date('birthDay');
            $table->string('address',30);
            $table->string('addressNumber',10);
            $table->string('cellphone',20);
            $table->float('payment');
            $table->string('enabled',1)->default("1");
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
