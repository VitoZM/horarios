<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billings', function (Blueprint $table) {
            $table->unsignedInteger('readingId');
            $table->float('pricePerMeter');
            $table->integer('consumption');
            $table->date('billingDate');
            $table->float('billingPrice');
            $table->integer('discountPercentage');
            $table->float('totalBillingPrice');
            $table->string('status',1)->default("0");
            $table->dateTime('paymentDate')->nullable();
            $table->date('expirationDate');
            $table->primary('readingId');
            $table->foreign('readingId')->references('readingId')->on('readings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billings');
    }
}
