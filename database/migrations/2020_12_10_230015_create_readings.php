<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReadings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('readings', function (Blueprint $table) {
            $table->increments('readingId');
            $table->integer('measurerReading')->default(0);
            $table->string('type',1);
            $table->string('image',40)->default("default.jpg")->nullable();
            $table->text('description')->default("")->nullable();
            $table->unsignedInteger('measurerId');
            $table->unsignedInteger('userId');
            $table->timestamps();
            $table->foreign('userId')->references('userId')->on('users');
            $table->foreign('measurerId')->references('measurerId')->on('measurers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('readings');
    }
}
