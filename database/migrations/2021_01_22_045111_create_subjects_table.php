<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects', function (Blueprint $table) {
            $table->increments('subjectId');
            $table->string('name',40);
            $table->string('initials','10')->unique();
            $table->integer('HT')->default(0);
            $table->integer('HP')->default(0);
            $table->integer('HL')->default(0);
            $table->integer('HI')->default(0);
            $table->integer('HEI')->default(0);
            $table->integer('TH')->default(0);
            $table->integer('CR')->default(0);
            $table->text('description');
            $table->string('enabled',1)->default('1');
            $table->unsignedInteger('semesterId');
            $table->timestamps();
            $table->foreign('semesterId')->references('semesterId')->on('semesters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjects');
    }
}
