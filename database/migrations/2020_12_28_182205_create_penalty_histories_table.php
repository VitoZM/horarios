<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenaltyHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penalty_histories', function (Blueprint $table) {
            $table->increments('penaltyHistoryId');
            $table->string('status','1')->default('0');
            $table->text('description');
            $table->unsignedInteger('clientId');
            $table->unsignedInteger('penaltyTypeId');
            $table->foreign('clientId')->references('clientId')->on('clients');
            $table->foreign('penaltyTypeId')->references('penaltyTypeId')->on('penalty_types');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penalty_histories');
    }
}
